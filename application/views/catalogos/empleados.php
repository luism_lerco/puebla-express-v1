<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Personal</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Empleados</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/catalogos/alta_empleado" class="btn btn-warning">Agregar Empleado</a>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>
    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/catalogos/getEmpleados"
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<i class='ft-chevron-down' aria-hidden='true'></i>"
                },
                {"data": "id"},
                {"data": "nombre"},
                {"data": "direccion"},
                {"data": "telefono"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='mb-0 btn btn-sm btn-icon btn-primary edit'><i class='ft-edit'></i></button>\
                    <button type='button' class='mb-0 btn btn-sm btn-icon btn-danger delete'><i class='ft-trash' aria-hidden='true'></i></button>"
                }
            ]
        });

    }
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_empleado/'+data.id;
        });

        //Listener para eliminar
        $('#tabla tbody').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            $.ajax({
                type: "POST",
                traditional: true,
                url: "<?php echo base_url(); ?>index.php/catalogos/deleteFromCatalogo/empleados",
                data: {id: data.id},
                success: function (data) {
                    
                        swal({
                            title: 'Exito!',
                            text: "Se eliminó el empleado",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                window.location = "<?php echo base_url(); ?>index.php/catalogos/empleados/";
                            }
                        }).catch(swal.noop);
                    
                }
            });
        });

        load();
    });
</script>