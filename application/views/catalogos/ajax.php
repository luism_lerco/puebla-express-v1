<script>
    function getEstadosAjax() {
        $.ajax({
            type: "POST",
            traditional: true,
            url: "<?php echo base_url(); ?>index.php/catalogos/getEstados",
            data: {pais: $("#slcPais").val()},
            success: function (data) {
                $("#slcEstados").empty();
                $("#slcEstados").append(data).trigger('chosen:updated');
                <?php if (isset($cliente)) { ?>
                 $("#slcEstados").val("<?php echo $cliente->estado; ?>").trigger('chosen:updated'); 
                <?php } ?>
            }
        });
    }
</script>
