<style type="text/css">
    #mapa {
        width: 80%;
        height: 350px;
        margin-left: 10%;
    }
</style>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCfEKOtiXFIT9FExIw1-a-Lgn2SJFAxxX4"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/gmaps.js" type="text/javascript"></script>


<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php
            $title = "Alta";
            if (isset($sucursal)) {
                $title = "Edición";
            }
            ?>
            <div class="content-header"><?php echo $title; ?> de Sucursal</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="fv-form fv-form-bootstrap" id="form-sucursal" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Datos de la Sucursal</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre <span>*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="sucursal" class="form-control form-control-sm toupper" <?php if (isset($sucursal)) {
                echo "value='$sucursal->sucursal'";
            } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Teléfono <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="telefono" class="form-control form-control-sm" <?php if (isset($sucursal)) {
                echo "value='$sucursal->telefono'";
            } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Dirección <span class="required">*</span></h5>
                                    <div class="controls">
                                        <textarea name="direccion" id="direccion" class="form-control form-control-sm toupper" rows="3"><?php if (isset($sucursal)) {
                echo $sucursal->direccion;
            } ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>País <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select name="pais" id="slcPais" class="form-control form-control-sm chosen-select">
                                            <option value="mx">México</option>
                                            <option value="usa">Estados Unidos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Estado <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select name="estado" id="slcEstados" class="form-control form-control-sm chosen-select">

                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Encargado <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select name="encargado" class="form-control form-control-sm chosen-select">
                                            <?php
                                            foreach ($empleados as $empleado) {
                                                $selected = "";
                                                if ($sucursal->encargado == $empleado->id) {
                                                    $selected = "selected";
                                                }
                                                echo "<option value='$empleado->id' $selected>$empleado->nombre</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Clave <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="clave" class="form-control form-control-sm toupper" <?php if (isset($sucursal)) {
                echo "value='$sucursal->clave'";
            } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Costo por Libra-Tierra <span class="required">*</span></h5>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="number" step="any" name="costo_libra_tierra" class="form-control form-control-sm" <?php if (isset($sucursal)) {
                                            echo "value='$sucursal->costo_libra_tierra'";
                                        } ?>>
                                        <span class="input-group-addon">USD</span>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <h5>Costo por Libra-Aire <span class="required">*</span></h5>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="number" step="any" name="costo_libra_avion" class="form-control form-control-sm" <?php if (isset($sucursal)) {
                                            echo "value='$sucursal->costo_libra_avion'";
                                        } ?>>
                                        <span class="input-group-addon">USD</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Comisión <span class="required">*</span></h5>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="number" step="any" name="comision" class="form-control form-control-sm" <?php if (isset($sucursal)) {
                                            echo "value='$sucursal->comision'";
                                        } ?>>
                                        <span class="input-group-addon">USD</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Observaciones</h5>
                                    <div class="controls">
                                        <textarea type="text" name="observaciones" class="form-control form-control-sm" rows="3"><?php if (isset($sucursal)) {
    echo $sucursal->observaciones;
} ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section"><i class="ft-map-pin"></i> Seleccione la ubicación en el mapa</h4>
                        <div class="row">

                            <div id="mapa"></div>
                            <input type="text" id="coordenadas" name="coordenadas" hidden <?php if (isset($sucursal)) {
    echo "value='$sucursal->coordenadas'";
} ?>>
                            <br>
                        </div>
                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/sucursales" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn btn-warning" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".chosen-select").chosen({width: "100%",placeholder_text_single:"Selecciona una Opción"});
        $(".toupper").on("change",function(){
                $(this).val($(this).val().toUpperCase());
            });
<?php
$lat = 19.426734;
$lng = -99.168194;
if (isset($sucursal)) {
    if ($sucursal->coordenadas != "") {
        $coordenadas = explode(',', $sucursal->coordenadas);
        $lat = $coordenadas[0];
        $lng = $coordenadas[1];
    }
}
?>

        var map = new GMaps({
            el: '#mapa',
            lat: <?php echo $lat; ?>,
            lng: <?php echo $lng; ?>,
            zoom: 14,
            click: function (e) {
                var lat = e.latLng.lat();
                var lng = e.latLng.lng();
                map.removeMarkers();
                map.addMarker({
                    lat: lat,
                    lng: lng
                });
                $("#coordenadas").val(lat + "," + lng);
            }
        });

        $("#slcPais").on("change", function () {
            getEstadosAjax();
        });
        
        $("#direccion").on("change", function () {
            GMaps.geocode({
                address: $('#direccion').val(),
                callback: function(results, status) {
                  if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                  }
                }
              });
        });

        
        // VALIDACION

        $("#form-sucursal")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
<?php
if (isset($sucursal)) {
    echo "fd.append('id', $sucursal->id);";
}
?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdateToCatalogo/sucursales",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data == 1)
                            {
                                var texto = "Se insertó la Sucursal";
                                var url = "<?php echo base_url(); ?>index.php/catalogos/sucursales";

<?php
if (isset($sucursal)) { // Si es Edicion
    echo "texto='Se actualizó la Sucursal'; ";
    echo "url='" . base_url() . "index.php/catalogos/edicion_sucursal/" . $sucursal->id . "';";
}
?>

                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false,
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        sucursal: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un nombre de Sucursal"
                                }
                            }
                        },
                        /*direccion: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese una Dirección"
                                }
                            }
                        },*/
                        telefono: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Teléfono"
                                }
                            }
                        },
                        comision: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese la Comisión"
                                }
                            }
                        },
                        clave: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese la clave de sucursal"
                                }
                            }
                        },
                        costo_libra: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el Costo"
                                }
                            }
                        }
                    }
                });
        // FIN VALIDADOR
        
        //init

    <?php
    if (isset($sucursal)) {
        ?>
        $("#slcPais").val('<?php echo $sucursal->pais; ?>').trigger('chosen:updated');
        getEstadosAjax();
      <?php  
        if ($sucursal->coordenadas != "") {
            echo "map.addMarker({
                            lat: $lat,
                            lng: $lng
                        });";
        }
    ?>

        
        
    <?php
    } else {
        echo "getEstadosAjax();";
    }
    ?>
        
    });
    
    
    function getEstadosAjax() {
        $.ajax({
            type: "POST",
            traditional: true,
            url: "<?php echo base_url(); ?>index.php/catalogos/getEstados",
            data: {pais: $("#slcPais").val()},
            success: function (data) {
                $("#slcEstados").empty();
                $("#slcEstados").append(data).trigger('chosen:updated');
                <?php if (isset($sucursal)) { ?>
                $("#slcEstados").val(<?php echo $sucursal->estado; ?>).trigger('chosen:updated');
                <?php } ?>
            }
        });
    }

</script>


