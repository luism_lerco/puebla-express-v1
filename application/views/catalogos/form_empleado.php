
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php $title="Alta"; 
                if(isset($empleado)){
                    $title="Edición";
                }
            ?>
            <div class="content-header"><?php echo $title; ?> de Empleado</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" id="form-empleado" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Datos del Empleado</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->nombre'";} ?> >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Teléfono <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="telefono" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->telefono'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Dirección </h5>
                                    <div class="controls">
                                        <textarea name="direccion" class="form-control form-control-sm toupper" rows="3"><?php if(isset($empleado)){ echo $empleado->direccion;} ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Sucursal</h5>
                                    <div class="controls">
                                        <select name="sucursal_id"  class="form-control form-control-sm chosen-select">
                                            <option value="0">N/A</option>
                                            <?php foreach($sucursales as $s){
                                                $sel="";
                                                if(isset($empleado) && $empleado->sucursal_id==$s->id){
                                                    $sel="selected";
                                                }
                                                echo "<option value='$s->id' $sel>$s->sucursal</option>";
                                            ?>
                                            
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Correo Electrónico <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="email" name="correo" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->correo'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Usuario <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="usuario" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->usuario'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Password <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="password" name="password" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->password'";} ?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section"><i class="ft-unlock"></i> Seleccione los Permisos del Empleado</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Permisos del Sistema</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Personal</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_personal" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->permiso_personal==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Sucursales</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_sucursales" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->permiso_sucursales==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Clientes</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_clientes" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->permiso_clientes==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Envios</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_envios" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->permiso_envios==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Editar Envios</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_editar_envio" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->permiso_editar_envio==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Escaneo</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_escaneo" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->permiso_escaneo==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Reportes</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_reportes" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->permiso_reportes==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p>Permisos de Escaneo de Paquetes</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Recolectar de Sucursal</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="estatus_proceso1" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->estatus_proceso1==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Entregar a Matriz</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="estatus_matriz" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->estatus_matriz==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Recolectar de Matriz</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="estatus_proceso2" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->estatus_proceso2==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Finalizar Envio</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="estatus_finalizado" value="1" type="checkbox" <?php if(isset($empleado) && $permisos->estatus_finalizado==1){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/empleados" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn btn-warning" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function () {
         
         $(".toupper").on("change",function(){
                $(this).val($(this).val().toUpperCase());
            });
        // VALIDACION

        $("#form-empleado")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    <?php if(isset($empleado)){
                        echo "fd.append('id', $empleado->id);";
                    }?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdateEmpleado",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data==1)
                            {
                                var texto="Se insertó el Empleado";
                                var url="<?php echo base_url(); ?>index.php/catalogos/empleados";
                                
                                <?php if(isset($empleado)){ // Si es Edicion
                                    echo "texto='Se actualizó el Empleado'; ";
                                    echo "url='".base_url()."index.php/catalogos/edicion_empleado/".$empleado->id."';";
                                } ?>
  
                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        nombre: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Nombre"
                                }
                            }
                        },
                        telefono: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Teléfono"
                                }
                            }
                        },
                        /*direccion: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese una Dirección"
                                },
                                regexp: {
                                    regexp: /^[A-Z\u00C0-\u00FF\s]+$/,
                                    message: 'Introduce un nombre válido y usando solo Mayúsculas'
                                }
                            }
                        },*/
                        correo: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Correo"
                                }
                            }
                        },
                        usuario: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el nombre de Usuario"
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese la Contraseña"
                                }
                            }
                        }
                    }
                });
                // FIN VALIDADOR
    });

</script>
