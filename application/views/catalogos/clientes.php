<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Clientes</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Clientes</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/catalogos/alta_cliente" class="btn btn-warning">Agregar Cliente</a>
                        <div class="overflow-auto">
                            <table class="table table-striped table-hover table-responsive" id="tabla">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Telefonos</th>
                                        <th>Correo Electrónico</th>
                                        <th>Dirección</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>

    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/catalogos/getClientes"
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<i class='ft-chevron-down' aria-hidden='true'></i>"
                },
                {"data": "id"},
                {"data": "nombre"},
                {"data": "telefono"},
                {"data": "correo"},
                {"data": "direccion"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='mb-0 btn btn-sm btn-icon btn-info edit'><i class='ft-edit'></i></button>"
                }
            ]
        });

    }
    $(document).ready(function () {
        table = $('#tabla').DataTable();
        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_cliente/'+data.id;
        });
        load();
    });
</script>