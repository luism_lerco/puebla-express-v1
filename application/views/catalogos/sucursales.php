<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Sucursales</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Sucursales</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/catalogos/alta_sucursal" class="btn btn-warning">Agregar Sucursal</a>
                        <table class="table table-striped table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th>Sucursal</th>
                                    <th>Encargado</th>
                                    <th>Teléfono</th>
                                    <th>País</th>
                                    <th>Comisión</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>

    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/catalogos/getSucursales"
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<i class='ft-chevron-down' aria-hidden='true'></i>"
                },
                {"data": "id"},
                {"data": "sucursal"},
                {"data": "encargado"},
                {"data": "telefono"},
                {"data": "pais"},
                {"data": "comision"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='mb-0 btn btn-sm btn-icon btn-info edit'><i class='ft-edit' aria-hidden='true'></i></button>\
                    <button type='button' class='mb-0 btn btn-sm btn-icon btn-danger delete'><i class='ft-trash' aria-hidden='true'></i></button>"
                }
            ]
        });

    }
    $(document).ready(function () {
        table = $('#tabla').DataTable();
        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_sucursal/'+data.id;
        });
        //Listener para eliminar
        $('#tabla tbody').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            $.ajax({
	            type: "POST",
	            traditional: true,
	            url: "<?php echo base_url(); ?>index.php/catalogos/deleteFromCatalogo/sucursales",
	            data: {id: data.id},
	            success: function (data) {
	                
	                    swal({
	                        title: 'Exito!',
	                        text: "Se eliminó la sucursal",
	                        type: 'success',
	                        showCancelButton: false,
	                        allowOutsideClick: false
	                    }).then(function (isConfirm) {
	                        if (isConfirm) {
	                            window.location = "<?php echo base_url(); ?>index.php/catalogos/sucursales/";
	                        }
	                    }).catch(swal.noop);
	                
	            }
	        });
    	});

        load();
    });
</script>