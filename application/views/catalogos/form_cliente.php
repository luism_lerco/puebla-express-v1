<style type="text/css">
    #mapa {
        width: 80%;
        height: 350px;
        margin-left: 10%;
    }
</style>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCfEKOtiXFIT9FExIw1-a-Lgn2SJFAxxX4"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/gmaps.js" type="text/javascript"></script>


<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php
            $title = "Alta";
            if (isset($cliente)) {
                $title = "Edición";
            }
            ?>
            <div class="content-header"><?php echo $title; ?> de Cliente</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" id="form-cliente" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Datos del Cliente</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre(s) <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control form-control-sm toupper" <?php if (isset($cliente)) {
                echo "value='$cliente->nombre'";
            } ?> >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Apellido Paterno <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="apaterno" class="form-control form-control-sm toupper" <?php if (isset($cliente)) {
                echo "value='$cliente->apaterno'";
            } ?> >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Apellido Materno</h5>
                                    <div class="controls">
                                        <input type="text" name="amaterno" class="form-control form-control-sm toupper" <?php if (isset($cliente)) {
                echo "value='$cliente->amaterno'";
            } ?> >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Telefono <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="telefono" class="form-control form-control-sm" <?php if (isset($cliente)) {
                echo "value='$cliente->telefono'";
            } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Correo Electrónico</h5>
                                    <div class="controls">
                                        <input type="email" name="correo" class="form-control form-control-sm" <?php if (isset($cliente)) {
                echo "value='$cliente->correo'";
            } ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                                <div class="form-group">
                                    <h5>Dirección <span class="required">*</span></h5>
                                    <div class="controls">
                                        <textarea name="direccion" id="direccion" class="form-control form-control-sm toupper" rows="3"><?php if (isset($cliente)) {
                echo $cliente->direccion;
            } ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Código Postal <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="cp" class="form-control form-control-sm" <?php if (isset($cliente)) {
                echo "value='$cliente->cp'";
            } ?> >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>País <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select name="pais" id="slcPais" class="form-control form-control-sm chosen-select">
                                            <option value="mx">México</option>
                                            <option value="usa">Estados Unidos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Estado <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select name="estado" id="slcEstados" class="form-control form-control-sm chosen-select">

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section"><i class="ft-map-pin"></i> Seleccione la ubicación en el mapa</h4>
                        <div class="row">

                            <div id="mapa"></div>
                            <input type="text" id="coordenadas" name="coordenadas" hidden <?php if (isset($cliente)) {
                echo "value='$cliente->coordenadas'";
            } ?>>
                            <br>
                        </div>
                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/clientes" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn btn-warning" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".chosen-select").chosen({width: "100%",placeholder_text_single:"Selecciona una Opción"});
        $(".toupper").on("change",function(){
                $(this).val($(this).val().toUpperCase());
            });
<?php
$lat = 19.426734;
$lng = -99.168194;
$zoom= 15;
if (isset($cliente)) {
    if ($cliente->coordenadas != "") {
        $coordenadas = explode(',', $cliente->coordenadas);
        $lat = $coordenadas[0];
        $lng = $coordenadas[1];
    }
}
?>
        var map = new GMaps({
            el: '#mapa',
            lat: <?php echo $lat; ?>,
            lng: <?php echo $lng; ?>,
            zoom: <?php echo $zoom; ?>,
            click: function (e) {
                var lat = e.latLng.lat();
                var lng = e.latLng.lng();
                map.removeMarkers();
                map.addMarker({
                    lat: lat,
                    lng: lng
                });
                $("#coordenadas").val(lat + "," + lng);
            }
        });

        $("#slcPais").on("change", function () {
            getEstadosAjax();
        });
        
        $("#direccion").on("change", function () {
            GMaps.geocode({
                address: $('#direccion').val(),
                callback: function(results, status) {
                  if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                  }
                }
              });
        });

        //init
        getEstadosAjax();

        <?php
        if (isset($cliente)) {
            
             ?>
                $("#slcPais").val('<?php echo $cliente->pais; ?>').trigger('chosen:updated');
                getEstadosAjax();
              <?php 
            
            if ($cliente->coordenadas != "") {
                echo "map.addMarker({
                                lat: $lat,
                                lng: $lng
                            });";
            }
            
            ?>

           //.trigger('chosen:updated');

        <?php
        }
        ?>

        //validador
        $("#form-cliente")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
<?php
if (isset($cliente)) {
    echo "fd.append('id', $cliente->id);";
}
?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdateToCatalogo/clientes",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data == 1)
                            {
                                var texto = "Se insertó el Cliente";
                                var url = "<?php echo base_url(); ?>index.php/catalogos/clientes";

<?php
if (isset($cliente)) { // Si es Edicion
    echo "texto='Se actualizó el Cliente'; ";
    echo "url='" . base_url() . "index.php/catalogos/edicion_cliente/" . $cliente->id . "';";
}
?>

                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        nombre: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un nombre de Cliente"
                                }
//                                stringLength: {
//                                    min: 1,
//                                    max: 255,
//                                    message: "Mínimo 1, máximo 255 caracteres"
//                                },
                                
                            }
                        },
                        apaterno: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el apellido del Cliente"
                                }
                            }
                        },
                        direccion: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese una Dirección"
                                }
                            }
                        },
                        cp: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el CP"
                                }
                            }
                        },
                        telefono: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Teléfono"
                                }
                            }
                        }
                    }
                });
        //fin validador
    });



</script>

<?php
include 'ajax.php';
