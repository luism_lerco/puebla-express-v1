<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

<div class="main-content">
        <div class="content-wrapper">
            <section class="color-palette">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content-header mb-3">Listado de Envíos</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <h5><i class="ft-mail"></i> Listado de Envíos</h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <a href="<?php echo base_url(); ?>index.php/operaciones/nuevo_envio" class="btn btn-warning pull-right ">Nuevo Envío
                                        <i class="fa fa-plane"></i></a>
                                </div>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">Envíos USA-MX</a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">Envíos MX-USA</a>
                                </li>

                            </ul>
                            <div class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                                    <div style="overflow: scroll;">
                                        <table class="table table-striped table-responsive" id="tabla">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Tracking #</th>
                                                <th>Sucursal</th>
                                                <th>Piezas</th>
                                                <th>Fecha</th>
                                                <th>Destinatario</th>
                                                <th>Destino</th>
                                                <th>Peso Total</th>
                                                <th>Precio Total</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                                    <div style="overflow: scroll;">
                                        <table class="table table-striped table-responsive display" id="tabla2">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Folio</th>
                                                <th>Sucursal</th>
                                                <th>Piezas</th>
                                                <th>Fecha</th>
                                                <th>Destinatario</th>
                                                <th>Destino</th>
                                                <th>Peso Total</th>
                                                <th>Precio Total</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

<?php include("js_listado.php");