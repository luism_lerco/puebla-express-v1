
<script>

  function load() {
    var estatus = $("#slcEstatus").val();
    table.destroy();
    table = $('#tabla').DataTable({
      "bProcessing": true,
      "serverSide": true,
      "lengthMenu": ["All"],
      "ajax": {
        "url": "<?php echo base_url(); ?>index.php/operaciones/getData_envios/",
        type: "post",
        error: function(){
          $("#tabla").css("display","none");
        }
      },
      "columns": [
        {
          "className": 'details-control',
          "orderable": false,
          "data": null,
          "defaultContent": "<i class='ft-chevron-right' aria-hidden='true'></i>"
        },
        {"data": "folio"},
        {"data": "sucursal"},
        {"data": "no_piezas"},
        {"data": "fecha"},
        {"data": "cliente_r"},
        {"data": "estado_r"},
        {"data": "total_peso"},
        {"data": "total_precio"},
        {
          "data": null,
          "defaultContent": <?php if($this->session->userdata['p_editar_envio']) {?>
              "<button type='button' class='btn btn-sm btn-icon btn-success print1'><i class='ft-file-text'></i></button>\
              <button type='button' class='btn btn-sm btn-icon btn-warning print2'><i class='ft-tag'></i></button>\
              <button type='button' class='btn btn-sm btn-icon btn-info print3' id='btn-edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button>"
                <?php } else { ?>
              "<button type='button' class='btn btn-sm btn-icon btn-success print1'><i class='ft-file-text'></i></button>\
               <button type='button' class='btn btn-sm btn-icon btn-warning print2'><i class='ft-tag'></i></button>" <?php } ?>
        }
      ],
      "order": [[1, "asc"]],
      "pagingType": "full_numbers",
    });

  }

  function imprimir(tipo, id) {
    window.open('<?php echo base_url(); ?>index.php/formatos/' + tipo + '/' + id,
      'imprimir',
      'width=600,height=500');
  }

  function edit_envio(id_envio) {
      window.open('<?php echo base_url();?>index.php/operaciones/edit_envio/' + id_envio, '_self');
  }

  $(document).ready(function () {
    $('#table_id').DataTable();
    table = $('#tabla').DataTable();
    table2 = $('#tabla2').DataTable();
    table3 = $('#table_detail').DataTable();



    //Listener para impresion
    $('.table tbody').on('click', 'button.print1', function () {
      var tr = $(this).closest('tr');
      var row = table.row(tr);
      var data = row.data();
      console.log('data');
      console.log(tr);
      imprimir("ticket", data.id_envio);
    });
    //Listener para impresion
    $('.table tbody ').on('click', 'button.print2', function () {
      var tr = $(this).closest('tr');
      var row = table.row(tr);
      var data = row.data();
      console.log('data');
      console.log(tr);
      imprimir("etiqueta", data.id_envio);
    });

    // Listener para editar envio
    $('.table tbody').on('click', 'button.print3', function () {
       var tr = $(this).closest('tr');
       var row = table.row(tr);
       var data = row.data();
       edit_envio(data.id_envio);
    });


    // Listener para informacion detalla de fila
    $('.table tbody').on('click', 'td.details-control', function () {

      var tr = $(this).closest('tr');
      var row = table.row(tr);
      if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
      } else {
        // Open this row
        row.child(format(row.data())).show();
        tr.addClass('shown');
      }
    });



    function format(d) {
      // 'd' son los datos originales de la tabla (json)
      return '<table id="table_detail" class="table_detail" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; font-size: 12px; width: 100%;">'+
        '<tr>'+
        '<td>Cliente que envía</td>'+
        '<td>'+ d.cliente_e+'<br>Telefono:'+d.tel_e+'<br>Correo:'+d.correo_e+'<br>Direccion:' +d.direccion_e+' <br>Estado:' +d.estado_e+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Cliente que recibe</td>'+
        '<td>'+ d.cliente_r+'<br>Telefono:'+d.tel_r+'<br>Correo:'+d.correo_r+'<br>Direccion:' +d.direccion_r+' <br>Estado:' +d.estado_r+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td class="total_v">Valor declarado</td>'+
        '<td>'+d.total_valor+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Total de peso</td>'+
        '<td>'+d.total_peso+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Observaciones</td>'+
        '<td>'+d.observaciones+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Descripcion</td>'+
        '<td>'+d.descripcion+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Imprimir</td>'+
        '<td><a href="<?php echo base_url(); ?>index.php/formatos/envioDetalle/'+d.id_envio+'" target="_blank" >Imprimir</a></td>'+
        '</tr>'+
        '</table>'

    }


    $('.table tbody').on('click', 'input.test', function () {
      console.log('test Input');
      console.log($('#test').val())

    });

    $('#detail').click(function() {
      console.log('click');
    });


    function doFunction() {
      console.log('Test de boton')
    }

    load();
  });
</script>