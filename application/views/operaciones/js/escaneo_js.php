<script>
    Vue.use(VueResource);

    new Vue({
        el: '#app',
        data: {
            sendingExist: false,
            idEnvio: '',
            folioEnvio: '',
            viajeSelected: '',
            envio: '',
            statusSelected: '',
            showModal: false,
            movementsHistory: [],
            estatusList: '',
            repartoSelected: '',
            repartosDisponibles: '',
            statusInfo: {
                estatus: '',
                paqueteria: '',
                numGuia: '',
                idViaje: '3',
                idReparto: ''
            },
            enableButtonSave: true,
            isLoading: false,
            availableTravels: []
        },
        beforeCreate: function () {
            console.log("Before create");
            toastr.options = {
                "toastr.options.closeButton": true,
                "toastr.options.progressBar": false,
                "toastr.options.timeOut": "2000",
                "progressBar": true,
                "positionClass": "toast-top-right"
            };
        },
        methods: {
            getEnvio() {
              console.log('Get envio');
                this.getEstatusList();
                this.$http.get("<?php echo base_url(); ?>index.php/operaciones/searchEnvio/" + this.idEnvio)
                    .then(response => {
                        return response.json();
                    })
                    .then(data => {
                      this.idEnvio = '';
                      console.log(data);
                        if (data) {
                            this.envio = data[0];
                            console.log('data', this.envio);
                            console.log('viaje', this.envio.viaje_id);
                            console.log('estatus', this.envio.estatus);
                            this.statusSelected = this.envio.estatus;
                            this.statusInfo.estatus = this.envio.estatus;
                            this.statusInfo.paqueteria = this.envio.paqueteria;
                            this.statusInfo.numGuia = this.envio.no_guia;
                            this.statusInfo.idViaje = this.envio.viaje_id;
                            this.statusInfo.idReparto = this.envio.reparto_id;
                            this.viajeSelected = this.envio.viaje_id;
                            this.repartoSelected = this.envio.reparto_id;
                            console.log("data");
                            console.log(data);
                            console.log("repartoSelected __>");
                            console.log(this.repartoSelected);
                            this.getAvailableTravels();
                            this.getRepartosDisponibles();
                            this.getMovemenHistory(this.envio.id_envio);
                            this.sendingExist = true;
                        } else {
                            this.sendingExist = false;
                            toastr.error("No se encontró el envío", "Error");
                        }
                    })
            },
            getEnvioByFolio() {
              this.getEstatusList();
              this.envio = '';
              this.folioEnvio = this.idEnvio;
              this.$http.get("<?php echo base_url(); ?>index.php/operaciones/searchEnvioByFolio/" + this.folioEnvio)
                .then(response => {
                  return response.json();
                })
                .then(data => {
                  if (data) {
                    this.envio = data[ 0 ];
                    console.log('data', this.envio);
                    console.log('viaje', this.envio.viaje_id);
                    console.log('estatus', this.envio.status);
                    this.statusSelected = this.envio.estatus;
                    this.statusInfo.estatus = this.envio.estatus;
                    this.statusInfo.paqueteria = this.envio.paqueteria;
                    this.statusInfo.numGuia = this.envio.no_guia;
                    this.statusInfo.idViaje = this.envio.viaje_id;
                    this.statusInfo.idReparto = this.envio.reparto_id;
                    this.viajeSelected = this.envio.viaje_id;
                    this.repartoSelected = this.envio.reparto_id;
                    this.getAvailableTravels();
                    this.getRepartosDisponibles();
                    this.getMovemenHistory(this.envio.id_envio);
                    this.sendingExist = true;
                  } else {
                    this.sendingExist = false;
                    toastr.error("No se encontró el envío", "Error");
                  }
                })
            },
            getEnvioByBarcode() {
                this.getEnvio();
            },

            getMovemenHistory(id_envio) {
                this.$http.get("<?php echo base_url(); ?>index.php/envios/" + id_envio + "/movimientos")
                    .then(response => {
                        return response.json()
                    })
                    .then(data => {
                        this.movementsHistory = data;
                    })
            },
            changeStatus() {
                this.isLoading = true;
                this.idEnvio = this.envio.id_envio;
                console.log('Change estatus');
                console.log('Viaje seleccionado');
                console.log(this.viajeSelected);
                this.statusInfo.estatus = this.statusSelected;
                this.statusInfo.idViaje = this.viajeSelected;
                this.statusInfo.idReparto = this.repartoSelected;
                this.$http.post("<?php echo base_url(); ?>index.php/envios/updateStatus/" + this.idEnvio, this.statusInfo)
                    .then(response => {
                        this.isLoading = false;
                        this.getEnvio();
                        this.enableButtonSave = true;
                        this.successMessage("Envío se actualizo correctamente", "Success");
                        console.log(response)
                    }, error => {
                        console.log("Error al actualizar status", error);
                        toastr.error("Error al actualizar envio", "error");
                    })
            },
            enableButton() {
                this.enableButtonSave = false;
            },
            getAvailableTravels() {
                console.log('getAvailableTravels');
                this.$http.get("<?php echo base_url(); ?>index.php/viajes/disponibles")
                    .then(response => {
                        return response.json()
                    })
                    .then(data => {
                        console.log("viajeSelected");
                        console.log(this.viajeSelected);

                        if (this.viajeSelected === "" || this.viajeSelected === null) {
                            console.log("viajeSelected es igual a nulo");
                            console.log(data[0]);
                            this.viajeSelected = data[0].id;
                        }
                        this.availableTravels = data;
                    })
            },
            getRepartosDisponibles() {
                console.log("Repartos disponibles GET");
                this.$http.get("<?php echo base_url(); ?>index.php/repartos/availableTravels")
                    .then(response => {
                        return response.json();
                    })
                    .then(data => {
                        console.log("repartos disponibles data");
                        console.log(data);
                        if (this.repartoSelected === "" || this.repartoSelected === null || this.repartoSelected == 0) {
                            this.repartoSelected = data[0].id_reparto;
                        }
                        this.repartosDisponibles = data;

                    })
            },
            formatDate(date) {
                return moment(date).format("YYYY-MM-DD")
            },
            getLocalDate(date) {
                const stillUtc = moment.utc(date).toDate();
                return moment(stillUtc).local().format('YYYY-MM-DD HH:mm:ss');
            },
            successMessage(message, tittle) {
                toastr.success(message, tittle);
            },
            deleteSendingOfTravel(idEnvio) {
                console.log('Id de envio');
                console.log(idEnvio);
                this.$http.delete("<?php echo base_url(); ?>index.php/viajes/deleteSending/" + idEnvio)
                    .then(response => {
                            console.log("Envio actualizado, se elimino del viaje");
                            console.log(response);
                            this.getEnvio();
                            this.successMessage("Se elimino viaje del envio: " + this.envio.folio, "Success");
                        },
                        error => {
                            console.log('ocurrio un error al eliminar viaje')
                        })
            },
            showModal() {
                this.$refs.myModalRef.show()
            },
            hideModal() {
                this.$refs.myModalRef.hide()
            },
            getEstatusList() {
                this.$http.get("<?php echo base_url(); ?>index.php/envios/get_status_list")
                    .then(response => {
                        return response.json();
                    })
                    .then( data => {
                        this.estatusList = data;
                    })
            }
        }
    });
    // $("#lb_direccion").text(data[ 0 ][ 'direccion_r' ]);
    // map = new GMaps({
    //   el: '#mapa',
    //   lat: 19.426734,
    //   lng: -99.168194,
    //   zoom: 13
    // });


</script>

<!--operaciones/searchEnvio/(:id)-->