<script>
    $(document).ready(function () {
        
        $("#div_estatus").hide();
        $("#div_paqueteria_externa").hide();
        
        map = new GMaps({
            el: '#mapa',
            lat: 19.426734,
            lng: -99.168194,
            zoom: 13
        });
        
        $("#es_externo").on("change",function(){
            if ($("#es_externo").val()==0){
                $("#div_paqueteria_externa").hide();
            }
            else {
                $("#div_paqueteria_externa").show();
            }
        });

    });

    function getEnvio() {
        $.ajax({
            type: "POST",
            traditional: true,
            dataType: 'json',
            url: "<?php echo base_url(); ?>index.php/operaciones/searchEnvio",
            data: {id: $("#search_id").val()},
            success: function (data) {
                console.log(data);
               if (data == 0) {
                    $("#search_id").val("");
                    swal({
                        title: 'No se encontraron resultados',
                        text: "Verifique que el Folio sea el correcto",
                        type: 'error',
                        showCancelButton: false
                    });
                } else {
                    
                    $("#id_envio").val(data[0]['id_envio']);
                    $("#estatus").val(data[0]['estatus']);
                    $("#lb_no").text(data[0]['folio']);
                    $("#lb_destinatario").text(data[0]['cliente_r']);
                    $("#lb_telefono").text(data[0]['tel_r']);
                    $("#lb_correo").text(data[0]['correo_r']);
                    $("#lb_observaciones").text(data[0]['observaciones']);
                    $("#lb_direccion").text(data[0]['direccion_r']);
                    $("#lb_estatus").text(""+nombre_estatus(data[0]['estatus']));
                    
                    $("#lb_envio_a").text(data[0]['envio_a']);
                    $("#lb_paqueteria").text(data[0]['paqueteria']);
                    $("#lb_no_guia").text(data[0]['no_guia']);
                    
                    $("#btn_estatus").text(""+nombre_boton(data[0]['estatus']));

                    $("#tabla_paquetes").html(data[0]['paquetes']);


                    if(data[0]['coord_r']!=""){
                        var coord = (data[0]['coord_r'] + "").split(",");
                        map.removeMarkers();
                        map.setCenter(coord[0], coord[1]);
                        map.addMarker({
                            lat: coord[0],
                            lng: coord[1]
                        }); 
                    }
                    
                    
                    $("#div_estatus").show();
                    $("#btn_estatus").focus(); 
                } 
            }
        });
    }
    
    function cambiar_estatus(){
        var id_envio=$("#id_envio").val();
        var estatus=$("#estatus").val();
        
        switch(estatus){
            case "1":  avanzar_estatus(id_envio,2); break; //avanza a proceso
            case "2":  avanzar_estatus(id_envio,3); break; //avanza a matriz
            case "3":  //recolecta datos de envio a cliente despues de estar en matriz
                $("#btn_modal_matriz").attr("onclick","avanzar_estatus("+id_envio+",4)");
                $("#modal_matriz").modal('show');
                break;
            case "4":  avanzar_estatus(id_envio,5); break;
            case "5":  swal("Envío Finalizado", 'El envío ha finalizado', "success"); break;
            default: alert("ERROR"); break;
        }
        
    }
    
    function avanzar_estatus(id,estatus){
        var data={id: id, estatus: estatus};
        
        if(estatus==4){ //va a proceso matriz-cliente
            data["es_externo"]=$("#es_externo").val();
            data["envio_a"]=$("#tipo_envio").val();
            if(data["es_externo"]==1){
                data["paqueteria"]=$("#paqueteria").val();
                data["no_guia"]=$("#no_guia").val();
            }
            
        }
        $.ajax({
            type: "POST",
            traditional: true,
            url: "<?php echo base_url(); ?>index.php/operaciones/avanzarEstatus",
            data: data,
            success: function (data) {
                if(data>0){
                    $("#div_estatus").hide();
                    $("#div_paqueteria_externa").hide();
                    $("#no_guia").val("");
                    $("#modal_matriz").modal('hide');
                    swal("Ok!", 'El envío ha cambiado de estatus', "success");
                    $("#id_envio").val(""); 
                    $("#id_envio").focus(); 
                }
                else{
                    if(data==-1){
                        swal("Error!", 'No tiene permisos de cambiar el estatus', "error");
                    }
                    else{
                        swal("Error!", 'Intente nuevamente o contacte al administrador', "error");
                    }
                    
                }
            }
        });
    }
    
    function nombre_estatus(id){
        var status="";
        switch(id){
            case "1": status= '"Recolección"'; break;
            case "2": status= '"En Proceso"'; break;
            case "3": status= '"En Matriz"'; break;
            case "4": status= '"En Proceso"'; break;
            case "5": status= '"Finalizado"'; break;
        }
        
        return status;
    }
    
    function nombre_boton(id){
        var texto="";
        switch(id){
            case "1": texto= 'Cambiar estatus a "En Proceso"'; break;
            case "2": texto= 'Cambiar estatus a "En Matriz"'; break;
            case "3": texto= 'Cambiar estatus a "En Proceso"'; break;
            case "4": texto= 'Cambiar estatus a "Finalizado"'; break;
            case "5": texto= 'Envío Finalizado'; break;
        }
        
        return texto;
    }
    
    function enterEvent(e) {
        if (e.keyCode == 13) {
            getEnvio();
            return false;
        }
    }

</script>


