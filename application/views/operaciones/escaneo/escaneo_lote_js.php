<script>
    Vue.use(VueResource);
    new Vue({
        el: '#escaneo_lote',
        data: {
            isLoading: '',
            statusSelected: '9',
            statusChange: '',
            statusList: [],
            viajeSelected: '',
            viajesList: '',
            repartoSelected: '',
            repartoList: '',
            isReparto: false,
            paqueteria: '',
            numGuia: '',
            statusInfo: {
                estatus: '',
                paqueteria: '',
                numGuia: '',
                idViaje: '',
                idReparto: ''
            },
            idEnvio: ''
        },
        methods: {
            cambioReparto($reparto) {
              console.log("reparto");
              console.log($reparto);
            },
            changeStatus() {
                console.log('Id de envio a actualizar');
                console.log(this.idEnvio);
                console.log(this.idReparto);
                this.statusInfo.estatus = this.statusSelected;
                this.statusInfo.paqueteria = this.paqueteria;
                this.statusInfo.numGuia = this.numGuia;
                this.statusInfo.idViaje = this.viajeSelected;
                this.statusInfo.idReparto = this.repartoSelected;
                console.log('sending info');
                console.log(this.statusInfo);
                this.changeStatusRequest();
            },
            getStatus: function () {
                this.$http.get("<?php echo base_url(); ?>index.php/envios/status/available")
                    .then(response => {
                        console.log(response);
                    })
            },
            formatDate(date) {
                return moment(date).format("YYYY-MM-DD")
            },
            changeStatusRequest() {
                this.statusChange = '';
                this.isLoading = true;
                this.$http.post("<?php echo base_url(); ?>index.php/envios/updateStatus/" + this.idEnvio, this.statusInfo)
                    .then(response => {
                            console.log('Actualizacion exitosa');
                            console.log(response);
                            this.isLoading = false;
                            this.statusChange = 'ok';
                            this.idEnvio = '';
                        },
                        error => {
                            console.log('error', error);
                            this.isLoading = false;
                            this.statusChange = 'failed';
                            this.idEnvio = '';
                        })
            }
        },
        beforeCreate: function () {
            this.$http.get("<?php echo base_url(); ?>index.php/repartos/availableTravels")
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log("data 2");
                    console.log(data);
                    if (data.length >= 0) {
                        this.repartoList = data;
                        this.repartoSelected = data[0].id_reparto;
                    } else {
                        this.repartoSelected = ""
                    }
                });
            this.$http.get("<?php echo base_url(); ?>index.php/envios/status/available")
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log("data 1");
                    console.log(data);
                    this.statusList = data;
                    if (localStorage.getItem("reparto")) {
                        this.repartoSelected = localStorage.getItem("reparto");
                        posicionIndex = "";
                        data.forEach(element => {
                            if(element.nombre === "En reparto") {
                                posicionIndex = element.id;
                            }
                        });
                        this.statusSelected = posicionIndex;
                        localStorage.removeItem("reparto");
                    } else {
                        this.statusSelected = data[0].id;
                    }
                });

            this.$http.get("<?php echo base_url(); ?>index.php/viajes/disponibles")
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log('Lista de viajes');
                    console.log(data);
                    this.viajesList = data;
                    this.viajeSelected = data[0].id
                });
        }
    })
</script>