<script src="<?php echo base_url(); ?>app-assets/js/vue.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
<script src="<?php echo base_url(); ?>/app-assets/js/moment.min.js" type="text/javascript"></script>

<style>
    .status-response{
        width: 100%;
        height: 350px;
        text-align: left;
    }
    .input-id{
        display: flex;
    }
    .message{
        margin-left: 50px;
        color: #2AA514;
        font-size: 50px;
    }
    .error-message{
        margin-left: 50px;
        color: #C0392B;
        font-size: 50px;
    }
    .ok{
        width: 100%;
        text-align: center;
        font-weight: bold;
    }
    .failed{
        width: 100%;
        text-align: center;
        font-weight: bold;
    }
    .status-viaje{
        width: 40%;
    }
    .paqueteria{
        display: flex;
        width: 100%;
    }
    .paqueteria-name{
        width: 40%%;
    }
    .num-guia{
        width: 40%;
    }
    .loading{
        width: 100%;
        height: 260px;
        text-align: center;
    }
    .loading-text{
        font-size: 26px;
        color: #a00606;
        font-weight: bold;
    }
</style>
<div id="escaneo_lote">
    <div class="main-content">
        <div class="content-wrapper">
           <h1>Escaneo por Lote</h1>
            <p>Seleccione un estado con el cual quiere actualizar sus paquetes</p>
            <div class="form-group" style="width: 80%">
                <select v-model="statusSelected" class="form-control status-select" id="status-envio">
                    <option v-for="status in statusList" v-bind:value="status.id">{{status.nombre}}</option>
                </select>
            </div>
            <div v-show="statusSelected == 5" class="status-viaje  form-group">
                <label for="selected-viaje">Seleccione viaje</label>
                <select v-model="viajeSelected" class="form-control" style="width: 100%;">
                    <option v-for="viaje in viajesList" v-bind:value="viaje.id">{{formatDate(viaje.fecha_salida)}}</option>
                </select>
            </div>
            <div class="paqueteria form-group" v-show="statusSelected == 8">
                <div class="paqueteria-name">
                    <label for="paqueteria">Paqueteria</label>
                    <input v-model="paqueteria" class="form-group" id="paqueteria" style="width: 100%" placeholder="Paqueteria">
                </div>
                <div class="num-guia">
                    <label for="n-guia">Num. de guia</label>
                    <input v-model="numGuia" class="form-group" id="n-guia" style="width: 100%" placeholder="Num. Guía">
                </div>

            </div>

            <div v-show="statusSelected == 9" class="form-group">
               <label for="selected-reparto">Selecciona un reparto</label>
                <select v-model="repartoSelected" class="form-control w-50" v-bind:value="repartoSelected">
                    <option v-for="reparto in repartoList" v-bind:value="reparto.id_reparto" selected="selected"> {{ formatDate(reparto.fecha_salida) }} </option>
                </select>
            </div>

                <label for="input-paqueteria">Escanea tu código</label><br>
            <div class="input-id">
                <span> <i class="fa fa-3x fa-barcode"></i></span>
                <input v-model="idEnvio" class="form-group" style="width: 40%; height: 45px" id="input-paqueteria"  @keyup.enter="changeStatus()" placeholder="Codigo">
            </div><br><br>
            <div v-show="isLoading" class="loading">
                <img style="width: 100%; max-width: 180px" src="<?php echo base_url(); ?>app-assets/img/loading.gif">
                <p class="loading-text">Actualizando</p>
            </div>
            <div class="status-response">
                <div v-show="statusChange == 'ok'" class="ok">
                    <img style="width:100%; max-width: 130px" src="<?php echo base_url(); ?>app-assets/img/success.png">
                    <span class="message">Success</span>
                </div>
                <div v-show="statusChange == 'failed'" class="failed">
                    <img style="width:100%; max-width: 130px" src="<?php echo base_url(); ?>app-assets/img/error.png">
                    <span class="error-message">Error</span>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include 'escaneo_lote_js.php';