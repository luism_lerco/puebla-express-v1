<style type="text/css">
    #mapa {
        width: 80%;
        height: 300px;
        margin-left: 10%;
    }
</style>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCfEKOtiXFIT9FExIw1-a-Lgn2SJFAxxX4"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/gmaps.js" type="text/javascript"></script>


<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <div class="content-header">Escaneo de Envíos</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="fv-form fv-form-bootstrap" id="form-sucursal" method="post">
                        <h4 class="form-section"><i class="ft-search"></i> Rastreo de Envío</h4>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                    <span> <i class="fa fa-3x fa-barcode"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="controls">
                                        <input type="text" id='search_id' onkeypress="return enterEvent(event)" class="form-control" placeholder="Número de Envío">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <button type="button" onclick="getEnvio()" class="btn btn-danger">Rastrear <i class="ft-search"></i></button>
                            </div>
                        </div>
                        <h4 class="form-section"><i class="ft-map-pin"></i> Datos del Envío</h4>
                        <div id="div_estatus" class="row alert alert-dark mb-2" role="alert">
                            <div class="col-md-6">
                                Este envío se encuentra en estatus de <strong><span id="lb_estatus"></span></strong>.
                                <input id="id_envio" hidden />
                                <input id="estatus" hidden />
                            </div>
                            <div class="col-md-6">
                                <!--Acción requerida: <br>-->
                                <button id="btn_estatus" onclick="cambiar_estatus()" class="btn btn-danger" type="button"></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <dl class="row">
                                    <dt class="col-md-4">Folio:</dt><dd class="col-md-8" id="lb_no"></dd>
                                    <dt class="col-md-4">Destinatario:</dt><dd class="col-md-8" id="lb_destinatario"></dd>
                                    <dt class="col-md-4">Télefono:</dt><dd class="col-md-8" id="lb_telefono"></dd>
                                    <dt class="col-md-4">E-mail:</dt><dd class="col-md-8" id="lb_correo"></dd>
                                    <dt class="col-md-4">Observaciónes:</dt><dd class="col-md-8" id="lb_observaciones"></dd>
                                </dl>
                            </div>
                            <div class="col-md-6">
                                <dl class="row">
                                    <dt class="col-md-4">Tipo de envío:</dt><dd class="col-md-8" id="lb_envio_a"></dd>
                                    <dt class="col-md-4">Paqueteria:</dt><dd class="col-md-8" id="lb_paqueteria"></dd>
                                    <dt class="col-md-4">No. Guia:</dt><dd class="col-md-8" id="lb_no_guia"></dd>
                                </dl>
                                <a href="#paquetes" data-toggle="collapse"><strong>Mostrar Paquetes </strong> <i class="ft-chevron-down"></i></a>
                                <div id="paquetes" class="collapse">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Descripcion</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabla_paquetes">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section"><i class="ft-map-pin"></i> Dirección del Envío</h4>
                        <p><strong>Dirección:</strong></p>
                        <p id="lb_direccion"></p>
                        <div class="row">
                            <div id="mapa"></div>
                            <br>
                        </div>
                        <br><hr>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--MODAL SELECCION ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_matriz" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Datos de Envío</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>Paqueteria <span class="required">*</span></p>
                    <select id="es_externo" class="form-control form-control-sm">
                        <option value="0">PUEBLA EXPRESS</option>
                        <option value="1">EXTERNO</option>
                    </select>
                </div>
                <div class="form-group">
                    <p>Tipo de Envío <span class="required">*</span></p>
                    <select id="tipo_envio" class="form-control form-control-sm">
                        <option>A OCURRE</option>
                        <option>A DOMICILIO</option>
                    </select>
                </div>
                <div id="div_paqueteria_externa">
                    <div class="form-group">
                        <p>Paqueteria Externa<span class="required">*</span></p>
                        <select id="paqueteria" class="form-control form-control-sm">
                            <option>ESTAFETA</option>
                            <option>FEDEX</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <p>No. Guia <span class="required">*</span></p>
                        <input id="no_guia" class="form-control form-control-sm">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_modal_matriz" class="btn btn-danger">Enviar a Proceso</button>
                <input type="reset" class="btn btn-outline-secondary" data-dismiss="modal" value="close">
            </div>
        </div>
    </div>
</div>
<!--MODAL  ---------------------------------------------------------------------->

<?php
include 'js_escaneo.php';
