<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCfEKOtiXFIT9FExIw1-a-Lgn2SJFAxxX4" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-on="http://www.w3.org/1999/xhtml"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/gmaps.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/app-assets/js/vue.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/app-assets/js/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/app-assets/js/toastr/toastr.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>


<style type="text/css">
    .loading_gif{
        width: 100%;
        position: absolute;
        justify-content: left;
        display: grid;
    }
    .btn_estatus{
        width: 100%!important;
        height: 37px;
    }
    .na-viajes{
        color: red;
        font-weight: bold;
        font-size: 12px;
    }
    .delete-travel{
        font-size: 10px;
    }
</style>

<div id="app">

    <div class="main-content">
        <div class="content-wrapper">

            <div class="col-sm-12">
                <div class="content-header">Escaneo de Envíos</div>
            </div>


            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h4 class="form-section"><i class="ft-search"></i> Rastreo de Envío</h4>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                    <span> <i class="fa fa-3x fa-barcode"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="controls">
                                        <input v-model="idEnvio" @keyup.enter="getEnvioByBarcode()" type="text" id='search_id' class="form-control" placeholder="Número de Envío">
                                        <!--                                            <input type="text" id='search_id' onkeypress="return enterEvent(event)" class="form-control" placeholder="Número de Envío">-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <!--<button type="button" onclick="getEnvio()" class="btn btn-danger">Rastrear-->
                                <button type="button" @click="getEnvioByFolio()" onclick="" class="btn btn-danger">Rastrear
                                    <i class="ft-search"></i></button>
                            </div>
                        </div>

                        <h4 class="form-section"><i class="ft-map-pin"></i> Datos del Envío</h4>
                        <div v-show="sendingExist" class="informacion-envio">
                            <form class="fv-form fv-form-bootstrap">
                                <div id="" class="row alert alert-dark mb-2 status" role="alert">
                                    <div class="col-12 center"><p class="titles">Estado de Envio</p></div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <select v-model="statusSelected" @change="enableButton()" class="form-control status-select" id="status-envio" v-bind:value="statusSelected">
                                                <option v-for="estatus in estatusList" v-bind:value="estatus.id" selected> {{ estatus.nombre }} </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-6 btn-status-container" style="justify-content: center;display: flex;">
<!--                                        <div class="loading"><img src="--><?php //echo base_url();?><!--/app-assets/img/loading.gif.png"></div>-->
                                        <div v-show="isLoading"class="loading_gif">
                                            <img style="width: 35px;margin-left: 20px" src="<?php echo base_url(); ?>app-assets/img/loading.gif">
                                        </div>
                                        <button id="btn_estatus" @click="changeStatus()" :disabled="enableButtonSave"class="btn btn-danger btn_estatus" type="button">GUARDAR</button>
                                    </div>
                                    <div class="col-12">
                                        <div class="row" v-show="statusSelected == 8">
                                            <form class="fv-form fv-form-bootstrap">
                                                <div class="col-6">
                                                    <label for="input-paqueteria">Paqueteria</label>
                                                    <input v-model="statusInfo.paqueteria"  @change="enableButton()" id="input-paqueteria" class="form-control" placeholder="Paqueteria">
                                                </div>
                                                <div class="col-6">
                                                    <label for="input-paqueteria">N. de guia</label>
                                                    <input id="input-paqueteria" v-model="statusInfo.numGuia"  @change="enableButton()"class="form-control" placeholder="Número de guîa">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row" v-show="statusSelected == 5">
                                                <div class="col-6">
                                                    <label for="input-paqueteria">Seleccione Viaje</label>
                                                    <select v-model="viajeSelected" @change="enableButton()" class="form-control status-select" id="status-envio">
                                                        <option v-for="viaje in availableTravels" v-bind:value="viaje.id">{{ formatDate(viaje.fecha_salida)}}</option>
                                                    </select>
                                                    <span v-show="availableTravels.length == 0" class="na-viajes">* No hay viajes en fecha</span>
                                                </div>


                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row" v-show="statusSelected == 9">
                                            <div class="col-6">
                                                <label for="input-paqueteria">Fecha de reparto</label>
                                                <select v-model="repartoSelected" @change="enableButton()" class="form-control status-select" id="status-reparto" v-bind:value="repartoSelected">
                                                    <option v-for="reparto in repartosDisponibles" v-bind:value="reparto.id_reparto" selected="selected">{{ formatDate(reparto.fecha_salida)}}</option>
                                                </select>
                                                <span v-show="repartosDisponibles.length == 0" class="na-viajes">* No hay repartos en fecha disponibles</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="row">

                                <div class="col-12 center"><p class="titles">Destino</p></div>
                                <div class="col-12 text-center destino-envio">{{envio.direccion_r}}, {{envio.estado_recepcion}}</div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="titles col-12">Información</div>
                                <br><br>
                                <div class="col-md-6">
                                    <dl class="row">
                                        <dt class="col-md-4">Folio:</dt>
                                        <dd class="col-md-8" id="lb_no">{{envio.folio}}</dd>
                                        <dt class="col-md-4">Costo por libra:</dt>
                                        <dd class="col-md-8" id="lb_no">{{envio.costo_libra}}</dd>
                                        <dt class="col-md-4">V. Declarado:</dt>
                                        <dd class="col-md-8" id="lb_no">{{envio.total_valor}}</dd>
                                        <dt class="col-md-4">Destinatario:</dt>
                                        <dd class="col-md-8" id="lb_destinatario">{{envio.cliente_r}}</dd>
                                        <dt class="col-md-4">Direccion:</dt>
                                        <dd class="col-md-8" id="lb_descripcion">{{envio.direccion_r}}</dd>
                                        <dt class="col-md-4">Télefono:</dt>
                                        <dd class="col-md-8" id="lb_telefono">{{envio.tel_r}}</dd>
                                        <dt class="col-md-4">E-mail:</dt>
                                        <dd class="col-md-8" id="lb_correo">{{envio.correo_r}}</dd>
                                        <dt class="col-md-4">Observaciónes:</dt>
                                        <dd class="col-md-8" id="lb_observaciones">{{envio.observaciones}}</dd>
                                        <dt class="col-md-4">Id viaje:</dt>
                                        <dd v-show="envio.viaje_id != null" class="col-md-8" id="lb_viaje">{{envio.viaje_id}} &nbsp; <button data-toggle="modal" data-target="#confirmDeletModal" type="button" class="btn btn-danger delete-travel">Eliminar de viaje</button> </dd>
                                    </dl>
                                </div>
                                <div class="col-md-6">
                                    <dl class="row">
                                        <dt class="col-md-4">Tipo de envío:</dt>
                                        <dd class="col-md-8" id="lb_envio_a"> {{ envio.tipo_envio }}</dd>
                                        <dt class="col-md-4">Paqueteria:</dt>
                                        <dd class="col-md-8" id="lb_paqueteria">{{envio.paqueteria}}</dd>
                                        <dt class="col-md-4">No. Guia:</dt>
                                        <dd class="col-md-8" id="lb_no_guia">{{envio.no_guia}}</dd>
                                        <dt class="col-md-4">Peso</dt>
                                        <dd class="col-md-8">{{envio.total_peso}} lbs.</dd>
                                        <dt class="col-md-4">Número de piezas</dt>
                                        <dd class="col-md-8">{{envio.no_piezas}}</dd>
                                        <dt class="col-md-4">Bodega de Salida</dt>
                                        <dd class="col-md-8" id="lb_bodega_salida">{{envio.reparto_bodega_salida}}</dd>
                                    </dl>
                                    <a href="#paquetes" data-toggle="collapse"><strong>Mostrar Paquetes </strong>
                                        <i class="ft-chevron-down"></i></a>
                                    <div id="paquetes" class="collapse">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Descripcion</th>
                                            </tr>
                                            </thead>
                                            <tbody v-html="envio.paquetes" id="tabla_paquetes">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="titles col-12">Historial de Movimientos</div>
                                <br><br>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Movimiento</th>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">Usuario</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="history in movementsHistory">
                                        <td>{{history.movimiento}}</td>
                                        <td>{{getLocalDate(history.fecha)}}</td>
                                        <td>{{history.nombre }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br><br>
<!--                            <h4 class="form-section"><i class="ft-map-pin"></i> Dirección del Envío</h4>-->
<!--                            <p><strong>Dirección:</strong></p>-->
<!--                            <p id="lb_direccion"></p>-->
<!--                            <div class="row">-->
<!--                                <div id="mapa"></div>-->
<!--                                <br>-->
<!--                            </div>-->
                            <br>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--MODAL SELECCION ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_matriz" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Datos de Envío</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>Paqueteria <span class="required">*</span></p>
                    <select id="es_externo" class="form-control form-control-sm">
                        <option value="0">PUEBLA EXPRESS</option>
                        <option value="1">EXTERNO</option>
                    </select>
                </div>
                <div class="form-group">
                    <p>Tipo de Envío <span class="required">*</span></p>
                    <select id="tipo_envio" class="form-control form-control-sm">
                        <option>A OCURRE</option>
                        <option>A DOMICILIO</option>
                    </select>
                </div>
                <div id="div_paqueteria_externa">
                    <div class="form-group">
                        <p>Paqueteria Externa<span class="required">*</span></p>
                        <select id="paqueteria" class="form-control form-control-sm">
                            <option>ESTAFETA</option>
                            <option>FEDEX</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <p>No. Guia <span class="required">*</span></p>
                        <input id="no_guia" class="form-control form-control-sm">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_modal_matriz" class="btn btn-danger">Enviar a Proceso</button>
                <input type="reset" class="btn btn-outline-secondary" data-dismiss="modal" value="close">
            </div>
        </div>
    </div>
</div>
<!--MODAL  ---------------------------------------------------------------------->

<div class="modal fade" id="confirmDeletModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeletModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmDeletModalLabel">Eliminar Envio de Viaje</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Estas seguro que deseas eliminar el envio del viaje?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" @click="deleteSendingOfTravel(envio.id_envio)"> Eliminar</button>

            </div>
        </div>
    </div>
</div>

</div>



<?php
include 'js_escaneo.php';
include 'js/escaneo_js.php';
include 'css/escaneo_css.php';
