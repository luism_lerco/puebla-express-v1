<script>
  var costoLibraTiera = 0;
  var costoLibraAvion = 0;
  $(document).ready(function() {

    no_criterios = 1;

    get_costolibraTierra();
    get_costolibraAvion();
    getTipoEnvio();

    $('.pickadate').pickadate({
      formatSubmit: 'yyyy-mm-dd',
      monthsFull: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
      monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec' ],
      weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
      today: 'Hoy',
      clear: 'Limpiar',
      close: 'Cerrar'
    });

    $('#slc_sucursales').on('change', function() {
      get_costolibraTierra();
    });

    $('#total_valor').on('change', function() {
      calculatePrice();
      validateSendingType();
    });

    $('#total_peso').on('change', function() {
      calculatePrice();
      validateSendingType()
    });

    $('#cantidad_paquetes').on('change', function() {
      var cantidadPaquetes = $('#cantidad_paquetes').val();
      $('#no_piezas').val(cantidadPaquetes)
    });


  });
  $('#tipo_envio').on('change', function() {
    if ($("#total_peso").val()) {
      console.log('peso esta definido');
      getTipoEnvio();
      calculatePrice();
      validateSendingType();
    } else {
      getTipoEnvio();
    }

  });

  function validateSendingType() {
    var total_peso = 0;
    total_peso = $("#total_peso").val();
    var tipoEnvio = $("#tipo_envio").val();
    setValidWeight();
    if (tipoEnvio === 'TIERRA' && total_peso < 20) {
      setInvalidWeight(20);
    }
    if (tipoEnvio === 'AIRE' && total_peso < 5) {
      setInvalidWeight(5);
    }
  }

  function calculatePrice() {
    console.log('change total_peso');
    var total_precio = 0;
    var precioValorDeclarado = 0;
    var costoLibra = 0;
    if ($("#tipo_envio").val() === 'TIERRA') {
      costoLibra = costoLibraTiera;
      $("#costo_libra").val(costoLibra);
    }
    if ($("#tipo_envio").val() === 'AIRE') {
      costoLibra = costoLibraAvion;
      $("#costo_libra").val(costoLibra);
    }
    var total_peso = ($("#total_peso").val() * costoLibra);
    precioValorDeclarado = calculatePriceByValue($("#total_valor").val());
    total_precio = precioValorDeclarado + total_peso;
    $("#total_precio").val(total_precio.toFixed(2));
  }

  function setInvalidWeight(peso) {
    console.log('El peso es menor a', peso);
    $("#label-mesagge").empty().append('*Peso minimo ' + peso + ' libras').css("color", "red");
    $("#total_peso").css("borderColor", "red");
    $("#btn-save-sending").prop('disabled', true);
    $("#total_precio").val("NA");
  }

  function setValidWeight() {
    $("#label-mesagge").empty();
    $("#total_peso").css("borderColor", "green");
    $("#btn-save-sending").prop('disabled', false);
  }

  function calculatePriceByValue(total_valor) {
    var rango = 100;
    var valueToMultiplicate = 0;
    var totalValue = 0;
    console.log('total_valor');
    console.log(total_valor);
    if (total_valor !== '0') {
      console.log('Dentro de if');
      valueToMultiplicate = 1;
      while (total_valor > rango) {
        valueToMultiplicate++;
        rango = rango + 100
      }
      totalValue = valueToMultiplicate * 6;
    }

    return totalValue;


  }

  function getTipoEnvio() {
    console.log('getTipoEnvio');
    var tipoEnvio = '';
    $("#label-mesagge").empty();
    tipoEnvio = $("#tipo_envio").val();
    if (tipoEnvio === 'TIERRA') {
      $("#label-mesagge").append('Peso minimo 20 libras');
    }
    if (tipoEnvio === 'AIRE') {
      $("#label-mesagge").append('Peso minimo 5 libras');
    }
    console.log(tipoEnvio);
    return tipoEnvio;


  }


  function modal_busqueda(cliente) {
    $("#modal_busqueda").modal('show');
    $("#tipo_cliente").val(cliente);
  }

  function agregar_criterio() {
    if (no_criterios < 5) {
      var row_criterio = '<div class="row criterios-extra"> \
                                <div class="col-md-3"> \
                                    <div class="form-group">\
                                        <select class="form-control form-control-sm tipo_criterios">\
                                            <option value="nombre">Nombre</option>\
                                            <option value="apaterno">Apellido Paterno</option>\
                                            <option value="amaterno">Apellido Materno</option>\
                                            <option value="telefono">Teléfono</option>\
                                            <option value="correo">Correo Eléctronico</option>\
                                            <option value="direccion">Dirección</option>\
                                        </select>\
                                    </div>\
                                </div>\
                                <div class="col-md-8">\
                                    <div class="form-group">\
                                        <div class="controls">\
                                            <input type="text" class="form-control form-control-sm criterios" placeholder="Ingrese..." required>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="col-md-1">\
                                    <button type="button" onclick="eliminar_criterio(this)" class="btn btn-sm btn-raised btn-outline-danger"><i class="ft-minus"></i></button>\
                                </div>\
                            </div>';
      $('#contenedor_criterios').append(row_criterio);
      no_criterios++;
    }
    else {
      toastr.warning('Alcanzaste el máximo de criterios');
    }
  }

  function eliminar_criterio(btn) {
    btn.parentElement.parentElement.remove();
    no_criterios--;
  }


  function buscar_cliente() {
    var tipo_criterios = [];
    var criterios = [];
    $(".tipo_criterios").each(function(index) {
      if ($(this).val() != "")
        tipo_criterios.push($(this).val());
    });
    $(".criterios").each(function(index) {
      if ($(this).val() != "")
        criterios.push($(this).val());
    });
    $.ajax({
      type: "POST",
      traditional: true,
      url: "<?php echo base_url(); ?>index.php/operaciones/searchCliente",
      data: { tipo_criterios: JSON.stringify(tipo_criterios), criterios: JSON.stringify(criterios) },
      success: function(data) {
        $("#tabla-resultados").html(data);
      }
    });
  }

  function selecciona_cliente(btn) {
    var row = btn.parentElement.parentElement;
    var who = $("#tipo_cliente").val();
    var valido = true;
    if (who == "recibe") {
      //validar que no sean del mismo pais
      if ($("#envia_pais").val() == row.cells[ 5 ].innerHTML) {
        valido = false;
      }
    }
    if (valido) {
      $("#" + who + "_id").val(row.cells[ 0 ].innerHTML);
      $("#" + who + "_nombre").val(row.cells[ 1 ].innerHTML);
      $("#" + who + "_correo").val(row.cells[ 3 ].innerHTML);
      $("#" + who + "_telefono").val(row.cells[ 2 ].innerHTML);
      $("#" + who + "_direccion").val(row.cells[ 4 ].innerHTML);
      $("#" + who + "_pais").val(row.cells[ 5 ].innerHTML);
      console.log("ID");
      console.log(row.cells[ 0 ]);

      $("#tabla-resultados").html("");
      $(".criterios-extra").remove();
      $("#modal_busqueda").modal('hide');
    }
    else {
      toastr.error("No se pueden realizar envios a un mismo país");
    }


  }

  function get_costolibraTierra() {
    console.log(<?php echo $this->session->userdata("sucursal_id"); ?>);
    $.ajax({
      type: "POST",
      traditional: true,
      url: "<?php echo base_url(); ?>index.php/operaciones/getCostoLibraTierra",
      data: { sucursal_id:<?php echo $this->session->userdata("sucursal_id"); ?>},
      success: function(data) {
        costoLibraTiera = data;
        console.log('El costo por libra de la sucursal de acuerdo al empleado');
        console.log($("#costo_libra").val())
      }
    });

  }

  function get_costolibraAvion() {
    console.log(<?php echo $this->session->userdata("sucursal_id"); ?>);
    $.ajax({
      type: "POST",
      traditional: true,
      url: "<?php echo base_url(); ?>index.php/operaciones/getCostoLibraAvion",
      data: { sucursal_id:<?php echo $this->session->userdata("sucursal_id"); ?>},
      success: function(data) {
        costoLibraAvion = data;
        console.log('El costo por libra de la sucursal por avion');
        console.log(data)
      }
    });

  }

  function imprimir(tipo, id) {
    window.open('<?php echo base_url(); ?>index.php/formatos/' + tipo + '/' + id,
      'imprimir',
      'width=600,height=500');
  }
</script>

