
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php
            $title = "Agregar Envios";
            if($envio_detail) {
                $title = "Editar Envio";
            }
            ?>
            <div class="content-header"><?php echo $title ?></div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="fv-form fv-form-bootstrap" id="form-envio" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Datos del Destino</h4>
                        <div class="row">
                            <!--<div class="col-md-2"></div>-->
                            <div class="col-md-6">
<!--                                <div class="form-group">
                                    <p>Fecha <span class="required">*</span></p>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar-o"></span>
                                        </span>
                                        <input type='text' name="fecha" class="form-control form-control-sm pickadate" placeholder="Selecciona la fecha" />
                                    </div>
                                </div>-->
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <p>Tipo de Envío <span class="required">*</span></p>
                                    <div class="controls">
                                        <select name="tipo_envio" class="form-control form-control-sm" id="tipo_envio">
                                            <option value='TIERRA' <?php if(isset($envio_detail) && $envio_detail->tipo_envio == "TIERRA") {echo "selected";} ?> >Tierra</option>
                                            <option value='AIRE' <?php if(isset($envio_detail) && $envio_detail->tipo_envio == "AIRE") {echo "selected";} ?> >Aire</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <p><button type="button" class="btn-sm btn-raised btn-outline-secondary" onclick="modal_busqueda('envia')"><i class="ft-search"></i></button>
                                        Cliente (envía) <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" id="envia_nombre" class="form-control form-control-sm" disabled
                                               <?php if(isset($envio_detail)){
                                                   echo "value='$envio_detail->cliente_e'";
                                               }?>
                                        >
                                        <input type="text" name="cliente_envia" id="envia_id" hidden
                                            <?php
                                            if(isset($envio_detail)) {
                                                echo "value='$envio_detail->ce_id'";
                                            }
                                            ?>>
                                        <input type="text" id="envia_pais" hidden>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Correo Electrónico <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" id="envia_correo" class="form-control form-control-sm" disabled
                                            <?php if(isset($envio_detail)){
                                                echo "value='$envio_detail->correo_e'";
                                            }?>
                                        >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Teléfono <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" id="envia_telefono"  class="form-control form-control-sm" disabled
                                             <?php
                                             if(isset($envio_detail)) {
                                                 echo "value='$envio_detail->tel_e' ";
                                             }?>
                                        >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Dirección <span class="required">*</span></p>
                                    <div class="controls">
                                        <?php
                                        $direccionClienteEnvia = null;
                                        if(isset($envio_detail)) {
                                            $direccionClienteEnvia = $envio_detail->direccion_e;
                                        }
                                        ?>
                                        <textarea name="direccion" id="envia_direccion" class="form-control form-control-sm" rows="3" disabled><?php echo $direccionClienteEnvia; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 text-center" style=" margin-top: 10%; ">
                                <span class="" ><i class="fa fa-5x fa-arrow-circle-right"></i></span>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <p><button type="button" class="btn-sm btn-raised btn-outline-secondary" onclick="modal_busqueda('recibe')"><i class="ft-search"></i></button>
                                        Cliente (recibe) <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" id="recibe_nombre" class="form-control form-control-sm" disabled
                                               <?php
                                                if(isset($envio_detail)) {
                                                    echo "value='$envio_detail->cliente_r'";
                                                }
                                               ?>
                                        >
                                        <input type="text" name="cliente_recibe" id="recibe_id" hidden
                                               <?php
                                               if(isset($envio_detail)) {
                                                   echo "value='$envio_detail->cr_id'";
                                               }
                                               ?>
                                        >
                                        <input type="text" id="recibe_pais" hidden>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Correo Electrónico <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" id="recibe_correo" class="form-control form-control-sm" disabled
                                               <?php
                                                if(isset($envio_detail)) {
                                                    echo "value='$envio_detail->correo_r'";
                                                }
                                               ?>
                                        >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Teléfono <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" id="recibe_telefono" class="form-control form-control-sm" disabled
                                               <?php
                                                if(isset($envio_detail)) {
                                                    echo "value='$envio_detail->tel_r'";
                                                }
                                               ?>
                                        >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Dirección <span class="required">*</span></p>
                                    <div class="controls">
                                        <?php
                                        $direccionClienteRecibe = null;
                                        if(isset($envio_detail)) {
                                            $direccionClienteRecibe = $envio_detail->direccion_r;
                                        }
                                        ?>
                                        <textarea id="recibe_direccion" class="form-control form-control-sm" rows="3" disabled><?php echo $direccionClienteRecibe; ?></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <h4 class="form-section"><i class="ft-package"></i> Paquetes</h4>
                        <div id="contenedor_paquetes">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="cantidad_paquetes">Cantidad:</label>
                                                    <div class="controls">
                                                        <input type="number" id="cantidad_paquetes" name="cantidad_paquetes[]" class="form-control" placeholder="Cantidad"
                                                               <?php
                                                                    if(isset($envio_detail)){
                                                                        echo "value='$envio_detail->no_piezas'";
                                                                    }
                                                               ?>
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="total_valor">Total Valor:</label>
                                                    <div class="controls">
                                                        <input type="text" name="total_valor" id="total_valor" class="form-control calculo-valor" placeholder="Valor"
                                                               <?php
                                                                if(isset($envio_detail)) {
                                                                    echo "value='$envio_detail->total_valor'";
                                                                }
                                                               ?>
                                                        >
                                                        <input type="text" name="costo_libra" id="costo_libra" hidden>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="total_peso">Total Peso:</label>
                                                    <div class="controls">
                                                        <input type="text" name="total_peso" id="total_peso" class="form-control calculo-valor" placeholder="Peso"
                                                               <?php
                                                                if(isset($envio_detail)) {
                                                                    echo "value='$envio_detail->total_peso'";
                                                                }
                                                               ?>
                                                        ><br>
                                                        <p style="margin-top: -22px;font-size: 11px; color: lightslategray" class="label-mesagge" id="label-mesagge"></p>
                                                    </div>
                                                    <input type="text" id="no_piezas" name="no_piezas"  class="form-control" placeholder="Piezas" hidden>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="total_precio" style="font-weight: bold">Total Precio:</label>
                                                    <div class="controls">
                                                        <input type="text" name="total_precio" id="total_precio" class="form-control" placeholder="Precio" disabled
                                                               <?php
                                                               if(isset($envio_detail)){
                                                                   echo "value='$envio_detail->total_precio'";
                                                               }
                                                               ?>
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="description">Descripcion:</label>
                                            <div class="controls">
                                                <?php
                                                $description = null;
                                                if(isset($envio_detail)){
                                                    $description = $envio_detail->descripcion;
                                                }
                                                ?>
                                                <textarea id="description" style="width: 100%;height: 231px;" class="form-control" name="descripcion_paquetes[]" placeholder="Descripción"><?php echo $description; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        <h4 class="form-section"><i class="ft-edit-3"></i> Observaciones</h4>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <div class="controls">
                                    <?php
                                    $observacioes = null;
                                    if(isset($envio_detail)){
                                        $observacioes = $envio_detail->observaciones;
                                    }
                                    ?>
                                    <textarea class="form-control" name="observaciones" rows="3" placeholder="Observaciones"><?php echo $observacioes; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <p class="text-right">Seguro de Envío</p>  
                            </div>
                            <div class="col-md-2">
                                <label class="switch">
                                    <?php
                                    if(isset($envio_detail)) {
                                        if($envio_detail->seguro) {
                                            echo "<input name='seguro' class='calculo-valor' id='seguro' type='checkbox' value='1' checked>";
                                        } else {
                                            echo "<input name='seguro' class='calculo-valor' id='seguro' type='checkbox' value='1'>";
                                        }
                                    } else {
                                        echo "<input name='seguro' class='calculo-valor' id='seguro' type='checkbox' value='1' checked>";
                                    }
                                    ?>
                                    <span class="sliderN round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <?php
                                    $txtBtnSubmit = "Guardar y Envíar";
                                    if(isset($envio_detail)){
                                        $txtBtnSubmit = "Actualizar";
                                    }
                                ?>
                                <button type="submit"  id="btn-save-sending" class="btn btn-danger width-300" > <?php echo $txtBtnSubmit ?> <i class="ft-save"></i></button>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

<!--MODAL DE BUSQUEDA ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_busqueda" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Busqueda de Cliente</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>Criterios de Busqueda: </label>
                <form class="form" id="form-modal" method="post">
                    <input id="tipo_cliente" hidden>
                    <div id="contenedor_criterios">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control form-control-sm tipo_criterios">
                                        <option value="nombre">Nombre</option>
                                        <option value="apaterno">Apellido Paterno</option>
                                        <option value="amaterno">Apellido Materno</option>
                                        <option value="telefono">Teléfono</option>
                                        <option value="correo">Correo Eléctronico</option>
                                        <option value="direccion">Dirección</option>
                                    </select>
                                </div> 
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="controls">
                                        <input type="text" class="form-control form-control-sm criterios" placeholder="Ingrese..." required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button type="button" onclick="agregar_criterio()" class="btn btn-sm btn-raised btn-outline-primary"><i class="ft-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    <button type="button" onclick="buscar_cliente()" class="btn btn-primary" style="width: 20%; margin-left: 80%;">Buscar</button>
                </form>
                <br><hr>
                <label>Resultados: </label>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Telefono</th>
                                <th>Correo Electrónico</th>
                                <th>Direccíon</th>
                                <th>País</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tabla-resultados">

                        </tbody>
                    </table>
                </div>
                <br><hr>
                <p>Si no encuentras el cliente dalo de alta <a target="a_blank" href="<?php echo base_url(); ?>index.php/catalogos/alta_cliente">aqui</a></p>
            </div>
            <div class="modal-footer">
                <input type="reset" class="btn btn-outline-secondary" data-dismiss="modal" value="close">
            </div>
        </div>
    </div>
</div>
<!--MODAL DE BUSQUEDA ---------------------------------------------------------------------->

<script>
    $(document).ready(function () {
        $("#optionTemplate").hide();
        //validador
        $("#form-envio")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();

                    if ($("#envia_id").val() != "" && $("#recibe_id").val() != "") {
                        var $form = $(e.target);
                        // Use Ajax to submit form data
                        var fd = new FormData($form[0]);
                        fd.append("sucursal_id",<?php echo $this->session->userdata("sucursal_id"); ?>);
                        fd.append("total_precio",$("#total_precio").val());
                        <?php
                        if(isset($envio_detail)) {
                            echo "fd.append('id_envio', $envio_detail->id_envio)";
                        }
                        ?>

                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>index.php/operaciones/insertOrUpdateEnvio",
                            cache: false,
                            data: fd,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                if (data > 0)
                                {
                                    var url = "<?php echo base_url(); ?>index.php/operaciones/nuevo_envio";
                                    var texto = "";
                                    //texto+="Se ha guardado el registro de envío";
                                    <?php
                                        if(isset($envio_detail)){
                                            echo "texto+='Se han actualizado los datos del envío';";
                                            echo "url='" . base_url() . "index.php/operaciones/edit_envio/" . $envio_detail->id_envio . "';";
                                        }
                                        else {
                                            echo "texto+='Se ha guardado el registro de envío';";
                                        }
                                    ?>
                                    texto+="<br><br>";
                                    texto+="<a class='btn btn-sm btn-danger white' onclick=imprimir('ticket','"+data+"')>Imprimir Ticket</a> ";
                                    texto+="<a class='btn btn-sm btn-warning white' onclick=imprimir('etiqueta','"+data+"')>Imprimir Etiqueta</a>";

                                    swal({
                                        title: 'Exito!',
                                        text: texto,
                                        type: 'success',
                                        showCancelButton: false,
                                        allowOutsideClick: false
                                    }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            window.location = url;
                                        }
                                    }).catch(swal.noop);
                                }
                            }
                        });

                     ;// fin ajax
                    } else {
                        console.log("Seleccione un Cliente");
                    }

                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
//                        folio: {
//                            validators: {
//                                notEmpty: {
//                                    message: "Por favor ingrese el Folio"
//                                }
//                            }
//                        },
//                        fecha: {
//                            validators: {
//                                notEmpty: {
//                                    message: "Por favor ingrese la fecha"
//                                }
//                            }
//                        },
                        'cantidad_paquetes[]': {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese una cantidad"
                                },
                                greaterThan: {
                                    value: 1,
                                    message: 'La cantidad minima debe ser 1'
                                }
                            }
                        },
                        'descripcion_paquetes[]': {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese una descripcion"
                                }
                            }
                        },
                        total_valor: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el total del valor"
                                }
                            }
                        },
                        total_peso: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el total del peso"
                                }
                            }
                        },
//                        total_precio: {
//                            validators: {
//                                notEmpty: {
//                                    message: "El total del precio no puede estar vacio"
//                                }
//                            }
//                        },
                        piezas: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el numero de piezas"
                                }
                            }
                        },
                        tipo_envio: {
                            validators: {
                                notEmpty: {
                                    message: "Requerido"
                                }
                            }
                        },
                        sucursal_id: {
                            validators: {
                                notEmpty: {
                                    message: "Requerido"
                                }
                            }
                        }
                    }
                }).on('err.field.fv', function (e, data) {
            if (data.fv.getSubmitButton()) {
                data.fv.disableSubmitButtons(false);
            }
        })
                .on('success.field.fv', function (e, data) {
                    if (data.fv.getSubmitButton()) {
                        data.fv.disableSubmitButtons(false);
                    }
                })
                // Add button click handler
                .on('click', '.addButton', function () {
                    var $template = $('#optionTemplate'),
                            $clone = $template
                            .clone()
                            .show()
                            .removeAttr('id')
                            .insertBefore($template),
                            $cantidad = $clone.find('[name="cantidad_paquetes[]"]'),
                            $descripcion = $clone.find('[name="descripcion_paquetes[]"]');

                    // Add new field
                    $('#form-envio').formValidation('addField', $cantidad);
                    $('#form-envio').formValidation('addField', $descripcion);
                })

                // Remove button click handler
                .on('click', '.removeButton', function () {
                    var $row = $(this).parents('.row'),
                            $cantidad = $row.find('[name="cantidad_paquetes[]"]'),
                            $descripcion = $row.find('[name="descripcion_paquetes[]"]');

                    // Remove element containing the option
                    $row.remove();

                    // Remove field
                    $('#form-envio').formValidation('removeField', $cantidad);
                    $('#form-envio').formValidation('removeField', $descripcion);
                });
        //fin validador 
    });

</script>


<?php
include 'js_envio.php';
