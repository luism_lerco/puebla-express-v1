<style type="text/css">
    #mapa {
        width: 80%;
        height: 300px;
        margin-left: 10%;
    }

    .btn-status-container {
        text-align: center;
    }

    .btn_estatus {
        width: 65%;
    }

    .destino-envio {
        color: #d2081f;
        font-weight: bold;
        font-size: 20px;
    }

    .status-select {
        width: 85%;
    }

    .titles {
        font-size: 20px;
        font-weight: bold;
        text-align: center;
    }

</style>
<?php
