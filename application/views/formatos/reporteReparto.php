<script src="<?php echo base_url(); ?>/app-assets/js/vue.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/app-assets/js/moment.min.js" type="text/javascript"></script>
<style type="text/css">
    table {
        border: solid 1px black;
        font-size: 9px;
        width: 100%;
    }

    td {
        border: solid 1px black;
    }

    .table-report {
        display: flex;
        justify-content: center;
        width: 100%;
        text-align: center;
    }
</style>

<div class="table-report" id="report">
    <h2 style="text-align: center">Reporte de repartos. Reparto de <?php echo $reparto[0]->fecha_salida; ?></h2><br>
    <table style="display: flex; justify-content: center;margin: 0 auto;">
        <tr>
            <td width="16%">Tracking #</td>
            <td width="17%">Recibe</td>
            <td width="17%">Dirección</td>
            <td width="16%">No. de Piezas</td>
            <td width="16%">Bodega salida</td>
            <td width="17%">Conductor</td>
        </tr>
        <?php
        $total_repartos = sizeof($envios);
        ?>
        <?php foreach ($envios as $envio) {
            ?>
            <tr>
                <td><?php echo $envio->folio; ?></td>
                <td><?php echo $envio->apaterno." ".$envio->amaterno." "." ".$envio->nombre; ?></td>
                <td><?php echo $envio->direccion; ?></td>
                <td><?php echo $envio->no_piezas; ?></td>
                <td><?php echo $envio->bodega_salida; ?></td>
                <td><?php echo $envio->conductor; ?></td>
            </tr>
        <?php }?>
        <tr style="font-weight: bold">
            <td>Num. envios</td>
            <td></td>
            <td><?php echo $total_repartos; ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>

<?php
/**
 * Created by IntelliJ IDEA.
 * User: qrsof
 * Date: 18/12/18
 * Time: 10:08
 */