<script src="<?php echo base_url(); ?>/app-assets/js/vue.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/app-assets/js/moment.min.js" type="text/javascript"></script>
<style type="text/css">
    table {
        border: solid 1px black;
        font-size: 9px;
        width: 100%;
    }

    td {
        border: solid 1px black;
    }

    .table-report {
        display: flex;
        justify-content: center;
        width: 100%;
        text-align: center;
    }
</style>

<div class="table-report" id="report">
    <h2 style="text-align: center">Reporte de envios. Viaje de <?php echo $viaje[0]->fecha_salida; ?></h2><br>
    <table style="display: flex; justify-content: center;margin: 0 auto;">
        <tr>
            <td width="14%">Tracking #</td>
            <td width="15%">Sucursal</td>
            <td width="12%">Bodega Destino</td>
            <td width="5%"># Paq.</td>
            <td width="34%">Descripción</td>
            <td width="10%">Peso</td>
            <td width="9%">Valor declarado</td>
        </tr>
        <?php
        $total_peso = 0;
        ?>
        <?php foreach ($envios as $envio) {
            $total_peso = $total_peso + $envio->total_peso;
            $total_paquetes = $total_paquetes + $envio->no_piezas;
            ?>
            <tr>
                <td><?php echo $envio->folio; ?></td>
                <td><?php echo $envio->sucursal; ?></td>
                <td><?php echo $envio->matriz; ?></td>
                <td><?php echo $envio->no_piezas; ?></td>
                <td><?php echo $envio->descripcion; ?></td>
                <td><?php echo $envio->total_peso; ?> lbs</td>
                <td><?php echo $envio->total_valor; ?> Dls</td>

            </tr>
        <?php }?>
        <tr style="font-weight: bold">
            <td>Total</td>
            <td></td>
            <td><?php echo $total_paquetes; ?></td>
            <td></td>
            <td></td>
            <td><?php echo $total_peso; ?> Lbs</td>
            <td></td>

        </tr>
    </table>
</div>
<?php
/**
 * Created by IntelliJ IDEA.
 * User: qrsof
 * Date: 18/10/18
 * Time: 18:24
 */
