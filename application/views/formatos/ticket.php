<?php
$color = "#OOOOOO";

?>
<table style="font-size: 9px">
    <tr>
        <td colspan="2" align="center">
            <img height="60px" src="<?php echo base_url(); ?>app-assets/img/logo_pe.png"/><br>
        </td>
<!--        <td colspan="2" align="center"><img height="60px" src="/app-assets/img/logo_pe2.png" /><br></td>-->
    </tr>
    <tr>
        <td colspan="2" align="center">
            <?php echo $envio->direccion_s; ?>
            <br>
            <?php echo $envio->fecha; ?>
            <br>
            Tracking # <?php echo $envio->folio; ?>

        </td>
    </tr>
    <tr>
        <td>No. Piezas: <?php echo $envio->no_piezas; ?></td>
        <td>Total $<?php echo $envio->total_precio; ?></td>
    </tr>
    <tr>
        <td align='center' colspan="2" style="background-color:<?php echo $color; ?>; color: white;">
            <strong>Paquetes del Envío</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td width="30%" style="font-weight: bold">Cantidad</td>
        <td width="70%" style="font-weight: bold">Descripcion</td>
    </tr>
    <tr style="font-size: 8px">
        <td><?php echo $envio->no_piezas; ?></td>
        <td><?php echo $envio->descripcion; ?></td>
    </tr>
    <br>
    <tr>
        <td width="30%" style="font-weight: bold">Peso</td>
        <td width="70%" style="font-weight: bold">Valor Declarado</td>
    </tr>
    <tr style="font-size: 8px">
        <td><?php echo $envio->total_peso; ?> Lbs</td>
        <td>$<?php echo $envio->total_valor; ?></td>
    </tr>
    <br>
    <tr>
        <td align='center' colspan="2" style="font-weight: bold">Observaciones
        </td>
    </tr>
    <tr>
        <td style="width: 30%"></td>
        <td style="width: 70%"><?php echo $envio->observaciones; ?></td>
    </tr>
    <tr>
        <td align='center' colspan="2" style="background-color:<?php echo $color; ?>; color: white;">
            <strong>Datos del Envío</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>

    <tr>
        <td align='center' colspan="2" style="background-color: #9C9B99; ">Cliente que Envía
        </td>
    </tr>

    <tr>
        <td>Cliente:</td>
        <td><?php echo $envio->cliente_e; ?></td>
    </tr>
    <tr>
        <td>Email:</td>
        <td><?php echo $envio->correo_e; ?></td>
    </tr>
    <tr>
        <td>Teléfono</td>
        <td><?php echo $envio->tel_e; ?></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td align='center' colspan="2" style="background-color: #9C9B99;">Cliente que Recibe
        </td>
    </tr>
    <tr>
        <td>Cliente:</td>
        <td><?php echo $envio->cliente_r; ?></td>
    </tr>
    <tr>
        <td>Email:</td>
        <td><?php echo $envio->correo_r; ?></td>
    </tr>
    <tr>
        <td>Teléfono:</td>
        <td><?php echo $envio->tel_r; ?></td>
    </tr>
    <tr>
        <td>Dirección:</td>
        <td><?php echo $envio->direccion_r; ?></td>
    </tr>
    <tr>
        <td>Estado:</td>
        <td><?php echo $envio->estado_recepcion; ?></td>
    </tr>
    <tr>
        <td>CP:</td>
        <td><?php echo $envio->cp; ?></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td align='center' colspan="2" style="background-color:<?php echo $color; ?>; color: white;">
            <strong>Politicas de Envío</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr style="font-size: 8px">
        <td colspan="2" align='justify'>1.-
            <strong>En ningún caso sera reintegrado el importe de su envío</strong>, solo el valor declarado
            <br>2.- No nos hacemos responsables por artículos no declarados
            <br>3.- El empaque no es responsabilidad nuestra
            <br>4.- El tiempo para verificar perdidas o extravios es de 60 dias
            <!--            1.- Cualquier artículo prohibido (drogas, armas, pornografía) será declarado ante las autoridades correspondientes.
                        <br>2.- El empaque es responsabilidad del cliente.
                        <br>3.- En caso de robo o extravio la empresa le resolvera en 40 dias como Maximo.
                        <br>4.-En ningún caso será reintegrado el costo de su envío.
                        <br>5.-Puebla Express se compromete a reembolsar el valor declarado en caso de pérdida total.
                        <br>6.-No nos hacemos responsables por artículos no declarados, mal empacados ni reclamos hechos después de que el destinatario salga de nuestras oficinas.-->
        </td>
    </tr>
    <tr>
        <td colspan="2"><br><br><br><br></td>
    </tr>
    <tr>
        <td colspan="2">
            <hr>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">Firma</td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2" align="center" style="font-size: 10px">Su envío a tiempo
            <br>www.puebla-express.com
        </td>
    </tr>
</table>
