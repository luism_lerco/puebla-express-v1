<div>
    <h1 style="text-align: center"><?php echo $envio->folio; ?></h1><br>
    <label>Sucursal: </label> <span><?php echo $matriz; ?></span><br>
    <label>Piezas: </label> <span><?php echo $envio->no_piezas; ?></span><br>
    <label>Fecha: </label> <span><?php echo $envio->fecha; ?></span><br>
    <label>Destinatario: </label> <span><?php echo $envio->cliente_r; ?></span><br>
    <label>Destino: </label> <span><?php echo $envio->estado_recepcion; ?></span><br>
    <label>Peso total: </label> <span><?php echo $envio->total_peso; ?></span><br>
    <label>Total de peso: </label> <span><?php echo $envio->total_peso; ?></span><br>
    <label>Valor Declarado: </label> <span><?php echo $envio->total_valor; ?></span><br>
    <label>Descripcion: </label> <span><?php echo $envio->descripcion; ?></span><br>
    <br>
    <table cellpadding="2" border="1">
        <tr>
            <th>Cliente que envía</th>
            <th>Cliente que recibe</th>
        </tr>
        <tr>
            <td style="font-size: 9px;padding: 5px">
                <?php echo $envio->cliente_e; ?><br>
                <label style="font-weight: bold">Teléfono: </label><?php echo $envio->tel_e; ?><br>
                <label style="font-weight: bold">Correo: </label><?php echo $envio->correo_e; ?><br>
                <label style="font-weight: bold">Dirección: </label><?php echo $envio->direccion_e; ?><br>
                <label style="font-weight: bold">Estado: </label><?php echo $envio->estado_origen; ?><br>
            </td>
            <td style="font-size: 9px;padding: 5px">
                <?php echo $envio->cliente_r; ?><br>
                <label style="font-weight: bold">Teléfono: </label><?php echo $envio->tel_r; ?><br>
                <label style="font-weight: bold">Correo: </label><?php echo $envio->correo_r; ?><br>
                <label style="font-weight: bold">Dirección: </label><?php echo $envio->direccion_r; ?><br>
                <label style="font-weight: bold">Estado: </label><?php echo $envio->estado_recepcion; ?><br>
            </td>
        </tr>
    </table>
</div>