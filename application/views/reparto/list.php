<style>
    .viajes .table {
        background-color: white;
    }

    #viaje-form .error {
        text-transform: inherit !important;
    }
</style>

<div id="reparto-content" class="main-content container-fluid">
    <div class="content-wrapper">
        <div>
            <h1>Listado de Repartos</h1>
            <div>
                <div class="form-group">
                    <label for="rage-dates">Fecha de salida</label>
                    <input id="rage-dates" type="text" value="">
                    <button class="btn btn-primary m-0" v-on:click="filterByDates">Buscar</button>
                    <button class="btn btn-primary m-0" v-on:click="listAll">Listar Todos</button>
                </div>

                <button class="btn btn-primary float-right" v-on:click="openNewForm">Agregar</button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Fecha de salida</th>
                    <th scope="col">Bodega de salida</th>
                    <th scope="col">Conductor</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                <tr v-if="repartos.length == 0">
                    <td>No hay repartos</td>
                </tr>
                <tr v-for="reparto in repartos">
                    <td> {{ reparto.fecha_salida }}</td>
                    <td> {{ reparto.bodega_salida }}</td>
                    <td> {{ reparto.conductor }}</td>
                    <td> {{ reparto.descripcion }}</td>
                    <td class="d-flex">
                        <a target="_blank"
                           v-bind:href="'<?php echo base_url(); ?>index.php/reportes/delivery_deals/'+reparto.id_reparto"
                           class="btn btn-flat m-0">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>

                        <div class="dropdown">
                            <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-th-list" aria-hidden="true"></i>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <button class="dropdown-item w-100" v-on:click="addReparto(reparto)" v-show="!isPastDate(reparto.fecha_salida)"><a href="<?php echo base_url(); ?>index.php/operaciones/escaneoLote/9" class="text-dark"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Agregar paquetes</a></button>
                                <button class="dropdown-item w-100" v-on:click="updateForm(reparto)"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button>
                                <button class="dropdown-item w-100" v-bind:reparto="reparto" v-on:click="deleteRepartoModal(reparto)"><i aria-hidden="true" class="fa fa-trash-o"></i> Borrar</button>
                            </div>
                        </div>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editRepartoModal" tabindex="-1" role="dialog" aria-labelledby="editRepartoModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editRepartoModalLabel">Reparto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="reporte-form">
                        <div class="form-group">
                            <label for="fecha-salida">Fecha de Salida</label>
                            <input v-model="repartoModel.fecha_salida" type="date" class="form-control"
                                   id="fecha-salida" name="fecha_salida" placeholder="Fecha de salida">
                        </div>
                        <div class="form-group">
                            <label for="bodega-salida">Bodega de Salida</label>
                            <input v-model="repartoModel.bodega_salida" type="text" class="form-control"
                                   id="bodega-salida" name="bodega_salida" placeholder="Bodega de salida">
                        </div>
                        <div class="form-group">
                            <label for="conductor">Conductor</label>
                            <input v-model="repartoModel.conductor" type="text" class="form-control" id="conductor"
                                   name="conductor" placeholder="Conductor">
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción</label>
                            <textarea v-model="repartoModel.descripcion" class="form-control" id="descripcion"
                                      name="descripcion" placeholder="Descripción"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" :disabled="submitting" v-on:click="checkFormReparto">
                        Guardar
                        <i v-if="submitting" class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!--    Modal -->
    <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar reparto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ¿Realmente quiere eliminar el reparto?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger" :disabled="submitting" v-on:click="doDeleteReparto">
                        Eliminar
                        <i v-if="submitting" class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
  (function () {
    Vue.component('delete-button', {
      props: [ 'reparto' ],
      data: function () {
        return {
          disabled: true
        }
      },
      created: function () {
        this.$http.get("--><?php echo base_url(); ?>/index.php/reparto/" + this.reparto.id + "/exists").then(res => {
            return res.json()
          }
        ).then((result) => {
          this.disabled = result[ 0 ].count === 0;
        });
      },
      template: '<button :disabled="disabled" class="btn btn-flat m-0" v-on:click="$emit(\'enlarge-text\')">\n' +
        '          <i class="fa fa-trash-o" aria-hidden="true"></i>\n' +
        '        </button>'
    });


    Vue.use(VueResource);
    const vueController = new Vue({
      el: '#reparto-content',
      data: {
        isUpdate: false,
        repartos: [],
        submitting: false,
        repartoModel: {
          id: "",
          fecha_salida: "",
          bodega_salida: "",
          conductor: "",
          descripcion: ""
        },
        repartoToDelete: undefined,
      },
      beforeCreate: function () {
        console.log("Before create");
        toastr.options = {
          "toastr.options.closeButton": true,
          "toastr.options.progressBar": false,
          "toastr.options.timeOut": "2000",
          "progressBar": true,
          "positionClass": "toast-top-right"
        };
      },
      created: function () {
        this.getRepartos();
      },
      methods: {
        filterByDates() {
          let dateRangeInput = $('#rage-dates').val().trim();
          if (dateRangeInput !== "") {
            var dates = dateRangeInput.split(" - ");
            var fechaInicial = moment(dates[ 0 ], "DD-MM-YYYY");
            var fechaFinal = moment(dates[ 1 ], "DD-MM-YYYY");
            console.log("rangeDates");
            this.$http.get("<?php echo base_url(); ?>index.php/repartos/searchByDates/fecha_salida/" + fechaInicial.format("YYYY-MM-DD") + "/" + fechaFinal.format("YYYY-MM-DD"))
              .then(response => {
                return response.json();
              })
              .then((data) => {
                if (data) {
                  console.log(data);
                  this.repartos = data;
                }
              })
          }
        },
        listAll() {
          $('#rage-dates').val("");
          this.getRepartos();
        },
        deleteRepartoModal(reparto) {
          this.submitting = false;
          $("#modal-eliminar").modal('show');
          console.log("reparto a borrar");
          console.log(reparto);
          this.repartoToDelete = reparto;
        },
        doDeleteReparto() {
          this.submitting = true;
          console.log("Eliminar");
          console.log(this.repartoToDelete);
          this.$http.delete("<?php echo base_url(); ?>index.php/repartos/deleteReparto/" + this.repartoToDelete.id_reparto)
            .then((res) => {
              console.log("this.repartoToDelete.id_reparto");
              console.log(this.repartoToDelete.id_reparto);
              console.log(res);
              this.submitting = false;
              $("#modal-eliminar").modal('hide');
              this.successMessage('Se elimino reparto','OK');
              this.getRepartos();
            }, (err) => {
              console.log("this.repartoToDelete.id error");
              console.log(this.repartoToDelete.id);
              console.log(err);
              $("#modal-eliminar").modal('hide');
              this.submitting = false;
              this.errorMessage(err.body,'Error');
            });
        },
        errorMessage(message, tittle) {
          toastr.error(message, tittle);
        },
        successMessage(message, tittle) {
          toastr.success(message, tittle);
        },
        cleanFormToAdd() {
          this.repartoModel.fecha_salida = "";
          this.repartoModel.bodega_salida = "";
          this.repartoModel.conductor = "";
          this.repartoModel.descripcion = "";
        },
        openNewForm() {
          this.cleanFormToAdd();
          $("#editRepartoModal").modal('show');
          this.isUpdate = false;
        },
        updateForm(reparto) {
          this.repartoModel = reparto;
          $("#editRepartoModal").modal('show');
          this.isUpdate = true;
        },
        getRepartos() {
          console.log("repartos.list");
          this.$http.get("<?php echo base_url(); ?>index.php/repartos")
            .then(response => {
              return response.json();
            })
            .then((data) => {
              if (data) {
                console.log(data);
                this.repartos = data;
              }
            }).catch(error => {
            console.log(error)
          })
        },

        checkFormReparto: function () {
          console.log("checkFormReparto");
          $("#reporte-form").submit();
        },

        saveReparto: function (form) {
          this.submitting = true;
          console.log("saveReparto");
          console.log(this.repartoModel);
          if (this.isUpdate) {
            this.$http.patch("<?php echo base_url(); ?>index.php/repartos", this.repartoModel, {
              emulateJSON: true
            }).then(response => {
              $(form).trigger("reset");
              $("#editRepartoModal").modal('hide');
              this.submitting = false;
              this.getRepartos();
              this.successMessage('Se modifico el reparto','OK');
            }, (error) => {
              console.log(error);
              this.submitting = false;
              this.errorMessage(err.body,'Error');
            });
          } else {
            this.$http.post("<?php echo base_url(); ?>index.php/repartos", this.repartoModel).then(response => {
              console.log("postReparto");
              console.log(response);
              $(form).trigger("reset");
              $("#editRepartoModal").modal('hide');
              this.repartos.push(this.repartoModel);
              this.submitting = false;
              this.getRepartos();
            }, (error) => {
              console.log(error);
              this.submitting = false;
            });
          }
        },
        isPastDate(dateReparto) {
            actualDate = moment().format("YYYY-MM-DD");
            return (dateReparto < actualDate)
        },
        formatDate(date) {
          return moment(date).format("YYYY-MM-DD")
        },
        addReparto(reparto) {
          localStorage.setItem("reparto", reparto.id_reparto);
        }
      }
    });

    $("#reporte-form").validate({
      debug: true,
      rules: {
        fecha_salida: {
          required: true
        },
        bodega_salida: {
          required: true
        },
        conductor: {
          required: true
        }
      },

      messages: {
        fecha_salida: {
          required: "Selecciona una fecha de salida"
        },
        bodega_salida: {
          required: "Ingrese una bodega de salida"
        },
        conductor: { required: "Por favor introduce el nombre del conductor" },
      },

      submitHandler: function (form) {
        console.log("submitHandler");
        vueController.saveReparto(form);
      }
    });

    $('#rage-dates').daterangepicker({
      autoUpdateInput: false,
    });

    $('#rage-dates').on('apply.daterangepicker', function (ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('#rage-dates').on('cancel.daterangepicker', function (ev, picker) {
      $(this).val('');
    });

  })();

</script>