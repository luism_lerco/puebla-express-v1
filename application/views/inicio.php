<div class="main-content">
    <div class="content-wrapper"><section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Inicio</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <a href="<?php 
                        $logueo = $this->session->userdata();
                        if($logueo['p_envios']){
                            echo base_url()."index.php/operaciones/envios"; 
                        }else{
                            echo "#";}
                    ?>">
                    <div class="card">
                        <div class="card-body">
                            <div class="px-3 py-3">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h3 class="mb-1">Envíos</h3>
                                    </div>
                                    <div class="media-right align-self-center">
                                        <i class="fa fa-envelope danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="<?php 
                        if($logueo['p_escaneo']){
                            echo base_url()."index.php/operaciones/escaneo"; 
                        }else{
                            echo "#";}
                    ?>">
                    <div class="card">
                        <div class="card-body">
                            <div class="px-3 py-3">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h3 class="mb-1">Escaneo</h3>
                                    </div>
                                    <div class="media-right align-self-center">
                                        <i class="fa fa-barcode danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </section>
    </div>
</div>

