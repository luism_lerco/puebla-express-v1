<body data-col="2-columns" class=" 2-columns ">
<div class="wrapper">


    <div data-active-color="white" data-background-color="black"
         data-image="<?php echo base_url(); ?>/app-assets/img/env.jpg" class="app-sidebar">
        <!--data-image="../app-assets/img/sidebar-bg/04.jpg"-->
        <div class="sidebar-header">
            <div class="logo clearfix" style="background-color: white">
                <a href="<?php echo base_url(); ?>index.php/main/inicio" class="logo-text float-left">
                    <div class="logo-img">
                        <img width="230px" src="<?php echo base_url(); ?>/app-assets/img/logo_pe.jpg"/>
                    </div><!-- <span class="align-middle">P. Express</span>-->
                </a>
                <!--<a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i data-toggle="expanded" class="ft-toggle-right toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a>-->
            </div>
        </div>
        <?php
        $logueo = $this->session->userdata();
        ?>
        <div class="sidebar-content">
            <div class="nav-container">
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/main/inicio"><i class="ft-home"></i><span
                                    data-i18n="" class="menu-title">Inicio</span></a>
                    </li>
                    <?php if ($logueo['p_personal'] || $logueo['p_sucursales'] || $logueo['p_clientes']) { ?>
                        <li class="has-sub nav-item">
                            <a href="#"><i class="ft-clipboard"></i><span data-i18n=""
                                                                          class="menu-title">Catálogos</span></a>
                            <ul class="menu-content">
                                <?php if ($logueo['p_sucursales']) { ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/catalogos/sucursales"
                                           class="menu-item">Sucursales</a>
                                    </li>
                                <?php }
                                if ($logueo['p_personal']) { ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/catalogos/empleados"
                                           class="menu-item">Empleados</a>
                                    </li>
                                <?php }
                                if ($logueo['p_clientes']) { ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/catalogos/clientes"
                                           class="menu-item">Clientes</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php }
                    if ($logueo['p_envios'] || $logueo['p_escaneo']) { ?>
                        <li class="has-sub nav-item">
                            <a href="#"><i class="ft-package"></i><span data-i18n=""
                                                                        class="menu-title">Operaciones</span></a>
                            <ul class="menu-content">
                                <?php if ($logueo['p_envios']) { ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/operaciones/envios"
                                           class="menu-item">Envíos</a>
                                    </li>
                                <?php }
                                if ($logueo['p_escaneo']) { ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/operaciones/escaneo"
                                           class="menu-item">Escaneo</a>
                                    </li>
                                <?php }
                                if ($logueo['p_escaneo']) { ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/operaciones/escaneoLote"
                                           class="menu-item">Escaneo por Lote</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php }
                    if ($logueo['p_reportes']) { ?>
                        <li class="has-sub nav-item">
                            <a href="#"><i class="ft-trending-up"></i><span data-i18n=""
                                                                            class="menu-title">Reportes</span></a>
                            <ul class="menu-content">
                                <li><a href="#" class="menu-item">Personal</a></li>
                                <li><a href="#" class="menu-item">Clientes</a></li>
                                <li><a href="#" class="menu-item">Envíos</a></li>
                                <li><a href="#" class="menu-item">Finanzas</a></li>
                            </ul>
                        </li>
                    <?php }
                    if ($logueo['p_envios']) { ?>
                        <li class="has-sub nav-item">
                            <a href="#"><i class="fa fa-plane" aria-hidden="true"></i><span data-i18n=""
                                                                                            class="menu-title">Viajes</span></a>
                            <ul class="menu-content">
                                <li>
                                    <a href="<?php echo base_url(); ?>index.php/viajes/list/view" class="menu-item">Listado</a>
                                </li>
                            </ul>
                        </li>
                    <?php }

                    if ($logueo['p_envios']) { ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>index.php/reparto/reparto_view"><i class="fa fa-truck"
                                                                                                 aria-hidden="true"></i><span
                                        data-i18n="" class="menu-title">Reparto</span></a>
                        </li>
                    <?php }
                    ?>


                </ul>

            </div>
        </div>
        <div class="sidebar-background"></div>
    </div>

    <div class="main-panel">

        <nav class="navbar navbar-expand-lg navbar-light bg-faded">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                                class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-container">
                    <div id="navbarSupportedContent" class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                            <li class="nav-item mr-2">
                                <a id="navbar-fullscreen" href="javascript:;" class="nav-link apptogglefullscreen"><i
                                            class="ft-maximize font-medium-3 blue-grey darken-4"></i>
                                    <p class="d-none">fullscreen</p></a></li>
                            <li class="dropdown nav-item">
                                <a id="dropdownBasic3" href="#" data-toggle="dropdown"
                                   class="nav-link position-relative dropdown-toggle"><i
                                            class="ft-user font-medium-3 blue-grey darken-4"></i>
                                    <p class="d-none">User Settings</p></a>
                                <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3"
                                     class="dropdown-menu dropdown-menu-right">
                                    <a href="javascript:;" class="dropdown-item py-1"><i
                                                class="ft-settings mr-2"></i><span>Configuración</span></a>
                                    <div class="dropdown-divider"></div>
                                    <a href="<?php echo base_url(); ?>index.php/main/cerrar_sesion"
                                       class="dropdown-item"><i class="ft-power mr-2"></i><span>Cerrar Sesión</span></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>