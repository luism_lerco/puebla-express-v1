<style>
    .viajes .table {
        background-color: white;
    }

    #viaje-form .error {
        text-transform: inherit !important;
    }

</style>

<div id="viajes-content" class="main-content viajes container-fluid">
    <div class="content-wrapper">
        <div>
            <h1>Listado de viajes</h1>
            <div>
                <div class="form-group">
                    <label for="rage-dates">Fecha de salida</label>
                    <input id="rage-dates" type="text" value="">
                    <button class="btn btn-primary m-0" v-on:click="filterByDates">Buscar</button>
                    <button class="btn btn-primary m-0" v-on:click="listAll">Listar Todos</button>
                </div>

                <button class="btn btn-primary float-right" v-on:click="openNewForm">Agregar</button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Fecha salida</th>
                    <th scope="col">Fecha llegada</th>
                    <th scope="col">Placas</th>
                    <th scope="col">Conductor</th>
                    <th scope="col">Observaciones</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                <tr v-if="viajes.length == 0">
                    <td>
                        No ha viajes registrados
                    </td>
                </tr>
                <tr v-for="viaje in viajes">
                    <td>{{ viaje.fecha_salida }}</td>
                    <td>{{ viaje.fecha_llegada }}</td>
                    <td>{{ viaje.placas }}</td>
                    <td>{{ viaje.conductor }}</td>
                    <td>{{ viaje.observaciones }}</td>
                    <td>
                        <button class="btn btn-flat m-0" v-on:click="updateForm(viaje)">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </button>
                        <a target="_blank"
                           v-bind:href="'<?php echo base_url(); ?>index.php/reportes/getPackagesFromTravel/'+viaje.id"
                           class="btn btn-flat m-0">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>

                        <delete-button v-bind:viaje="viaje" v-on:enlarge-text="deleteViajeModal(viaje)"></delete-button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Viajes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="viaje-form">
                        <div class="form-group">
                            <label for="fecha-salida">Fecha salida</label>
                            <input v-model="viajeModel.fecha_salida" type="date" class="form-control" id="fecha-salida"
                                   name="fecha_salida" placeholder="Fecha salida">
                        </div>
                        <div class="form-group">
                            <label for="fecha-llegada">Fecha llegada</label>
                            <input v-model="viajeModel.fecha_llegada" type="date" class="form-control"
                                   id="fecha-llegada" name="fecha_llegada" placeholder="Fecha llegada">
                        </div>
                        <div class="form-group">
                            <label for="placas">Placas</label>
                            <input v-model="viajeModel.placas" type="text" class="form-control" id="placas"
                                   name="placas" placeholder="Placas">
                        </div>
                        <div class="form-group">
                            <label for="conductor">Conductor</label>
                            <input v-model="viajeModel.conductor" type="text" class="form-control" id="conductor"
                                   name="conductor" placeholder="Conductor">
                        </div>
                        <div class="form-group">
                            <label for="observaciones">Observaciones</label>
                            <textarea v-model="viajeModel.observaciones" class="form-control" id="observaciones"
                                      name="observaciones" placeholder="Observaciones"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" :disabled="submitting" v-on:click="checkFormViaje">
                        Guardar
                        <i v-if="submitting" class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar viaje</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ¿Realmente quiere eliminar el viaje?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger" :disabled="submitting" v-on:click="doDeleteViaje">
                        Eliminar
                        <i v-if="submitting" class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    (function () {

        Vue.component('delete-button', {
            props: ['viaje'],
            data: function () {
                return {
                    disabled: true
                }
            },
            created: function () {
                this.$http.get("<?php echo base_url(); ?>index.php/viajes/" + this.viaje.id + "/envios/exists").then(res => {
                        return res.json()
                    }
                ).then((result) => {
                    this.disabled = result[0].count === 0;
                });
            },
            template: '<button :disabled="disabled" class="btn btn-flat m-0" v-on:click="$emit(\'enlarge-text\')">\n' +
                '          <i class="fa fa-trash-o" aria-hidden="true"></i>\n' +
                '        </button>'
        });


        Vue.use(VueResource);
        const vueController = new Vue({
            el: '#viajes-content',
            data: {
                isUpdate: false,
                viajes: [],
                submitting: false,
                viajeModel: {
                    id: "",
                    fecha_salida: "",
                    fecha_llegada: "",
                    placas: "",
                    conductor: "",
                    observaciones: "",
                },
                viajeToDelete: undefined,
            },
            beforeCreate: function () {
                console.log("Before create");
                toastr.options = {
                    "toastr.options.closeButton": true,
                    "toastr.options.progressBar": false,
                    "toastr.options.timeOut": "2000",
                    "progressBar": true,
                    "positionClass": "toast-top-right"
                };
            },
            created: function () {
                this.getViajes();
            },
            methods: {
                filterByDates() {
                    let dateRangeInput = $('#rage-dates').val().trim();
                    if (dateRangeInput !== "") {
                        var dates = dateRangeInput.split(" - ");
                        var fechaInicial = moment(dates[0], "DD-MM-YYYY");
                        var fechaFinal = moment(dates[1], "DD-MM-YYYY");
                        console.log("rangeDates");
                        this.$http.get("<?php echo base_url(); ?>index.php/viajes/searchByDates/fecha_salida/" + fechaInicial.format("YYYY-MM-DD") + "/" + fechaFinal.format("YYYY-MM-DD"))
                            .then(response => {
                                return response.json();
                            })
                            .then((data) => {
                                if (data) {
                                    console.log(data);
                                    this.viajes = data;
                                }
                            })
                    }
                },
                listAll() {
                    $('#rage-dates').val("");
                    this.getViajes();
                },
                deleteViajeModal(viaje) {
                    this.submitting = false;
                    $("#modal-eliminar").modal('show');
                    this.viajeToDelete = viaje;
                },
                doDeleteViaje() {
                    this.submitting = true;
                    this.$http.delete("<?php echo base_url(); ?>index.php/viajes/" + this.viajeToDelete.id).then(() => {
                        this.viajes.splice(this.viajes.indexOf(this.viajeToDelete), 1);
                        this.submitting = false;
                        $("#modal-eliminar").modal('hide');
                        this.successMessage('Se elimino el viaje', 'OK');
                    }, (err) => {
                        this.submitting = false;
                        this.errorMessage(err.body, 'Error');
                    });
                },
                cleanFormToAdd() {
                    this.viajeModel.fecha_salida = "";
                    this.viajeModel.fecha_llegada = "";
                    this.viajeModel.placas = "";
                    this.viajeModel.conductor = "";
                    this.viajeModel.observaciones = "";
                },
                openNewForm() {
                    this.cleanFormToAdd();
                    $("#exampleModal").modal('show');
                    this.isUpdate = false;
                },
                updateForm(viaje) {
                    this.viajeModel = viaje;
                    $("#exampleModal").modal('show');
                    this.isUpdate = true;
                },
                getViajes() {
                    this.$http.get("<?php echo base_url(); ?>index.php/viajes")
                        .then(response => {
                            return response.json();
                        })
                        .then((data) => {
                            if (data) {
                                this.getOnlyDate(data);
                                this.viajes = data;
                            }
                        })
                },
                getOnlyDate(viajes) {
                    viajes.forEach(viaje => {
                        viaje.fecha_salida = moment(viaje.fecha_salida).format("YYYY-MM-DD");
                        viaje.fecha_llegada = moment(viaje.fecha_llegada).format("YYYY-MM-DD");
                    });
                    return viajes;
                },
                checkFormViaje: function () {
                    console.log("checkFormViaje");
                    $("#viaje-form").submit();
                },

                saveViaje: function (form) {
                    this.submitting = true;
                    if (this.isUpdate) {
                        this.$http.patch("<?php echo base_url(); ?>index.php/viajes", this.viajeModel, {
                            emulateJSON: true
                        }).then(response => {
                            console.log("patchViaje");
                            console.log(response);
                            $(form).trigger("reset");
                            $("#exampleModal").modal('hide');
                            this.submitting = false;
                            this.successMessage('Se modifico el viaje', 'OK');
                        }, (error) => {
                            console.log(error);
                            this.submitting = false;
                            this.errorMessage(err.body, 'Error');
                        });
                    } else {
                        this.$http.post("<?php echo base_url(); ?>index.php/viajes", this.viajeModel).then(response => {
                            console.log("postViaje");
                            console.log(response);
                            $(form).trigger("reset");
                            $("#exampleModal").modal('hide');
                            this.submitting = false;
                            this.getViajes();
                        }, (error) => {
                            console.log(error);
                            this.submitting = false;
                        });
                    }
                },

                formatDate(date) {
                    return moment(date).format("YYYY-MM-DD")
                },
                errorMessage(message, tittle) {
                    toastr.error(message, tittle);
                },
                successMessage(message, tittle) {
                    toastr.success(message, tittle);
                }
            }
        });

        $("#viaje-form").validate({
            debug: true,
            rules: {
                fecha_salida: {
                    required: true
                },
                fecha_llegada: {
                    required: true
                },
                placas: {
                    required: true
                }
            },

            messages: {
                fecha_salida: {
                    required: "Selecciona una fecha de salida"
                },
                fecha_llegada: {
                    required: "Selecciona una fecha de llegada"
                },
                placas: {required: "Por favor introduce el número de placas"},
            },

            submitHandler: function (form) {
                console.log("submitHandler");
                vueController.saveViaje(form);
            }
        });

        $('#rage-dates').daterangepicker({
            autoUpdateInput: false,
        });

        $('#rage-dates').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        });

        $('#rage-dates').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

    })();

</script>