<?php

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');


class Empleados extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->model('Catalogos_model', 'catalogos');
    }

    public function index_get()
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
//            $data = $this->catalogos->getItemCatalogo("empleados",$id);
//            $this->response($data);
        } else {
            $this->response(null, 401);
        }
    }

    public function getEmployeeById_get($id) {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->catalogos->getItemCatalogo("empleados",$id);
            $this->response($data);
        }else {
            $this->response(null, 401);
        }
    }
}