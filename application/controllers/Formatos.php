<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Formatos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->model('Operaciones_model', 'model');
    }

    public function ticket($id_envio)
    {
        $data['envio'] = $this->model->getEnvio($id_envio);
        $data['Envios'] = $this->model->getPaquetes($id_envio);

        $this->printPDF("Ticket de Envío", 'ticket', $data);
        //$this->load->view('formatos/ticket', $data);
    }

    public function envioDetalle($id_envio)
    {
        $envio = $this->model->getEnvio($id_envio);
        $data['envio'] = $envio;
        $matriz = $this->model->getMatriz($envio->estado_c);
        $data['matriz'] = $matriz;
        $this->load->library('Pdf');
        $pdf = new TCPDF("P", PDF_UNIT, "LETTER", TRUE, 'UTF-8', FALSE);
        $pdf->SetTitle($envio->folio);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

        $pdf->AddPage();

        $pdf->Image('app-assets/img/logo_pe.jpg', 5, 5, 35, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        $html = $this->load->view('formatos/detalleEnvio', $data, true);
        $pdf->writeHTML($html, true, 0, true, 0);
        $pdf->lastPage();
        $pdf->Output("etiqueta" . '.pdf', 'I');
    }

    private function addFieldEnvioDetalle($pdf, $label, $value)
    {
        $pdf->Cell(25, 0, $label);
        $pdf->Multicell(0, 0, $value, 0, 1);
    }


    public function etiqueta($id_envio)
    {
        $envio = $this->model->getEnvio($id_envio);
        $matriz = "BODEGA " . substr($this->model->getMatriz($envio->estado_c), 0, 3);
        $paquetes = $this->model->getPaquetes($id_envio);

        $this->load->library('Pdf');

        $width = 29 * 1;
        $height = 90 * 1;
        $pageLayout = array($width, $height);

        $pdf = new TCPDF("Lanscape", PDF_UNIT, $pageLayout, TRUE, 'UTF-8', FALSE);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Puebla Express');
        $pdf->SetTitle("etiqueta");

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(0, 0, 0, true);

        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

        $pdf->AddPage();
        $pdf->SetFont('helvetica', '', 15);


        // define barcode style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 6,
            'stretchtext' => 4
        );


        $folio = $envio->id_envio;

        $pdf->Multicell(0, 0, $matriz . "\n\nTracking # " . $envio->folio, 0, 1);
        $pdf->write1DBarcode($folio, 'C39', '24', '11', '', 20, 0.6, $style, 'N');
//        foreach ($paquetes as $p) {
//
//            $i++;
//        }

        //$pdf->Cell(0, 0, "img", 0, 1);
        $pdf->Image('app-assets/img/logo_pe.jpg', 55, 0, 35, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);

        $pdf->Output("etiqueta" . '.pdf', 'I');
    }

    public function printPDF($name, $file, $data)
    {

        $this->load->library('Pdf');

        $width = 72;
        $height = 297;
        $pageLayout = array($width, $height);

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, $pageLayout, TRUE, 'UTF-8', FALSE);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Puebla Express');
        $pdf->SetTitle($name);

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(5, 5, 5, true);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


        $html = $this->load->view('formatos/' . $file, $data, true);
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $pdf->Output($name . '.pdf', 'I');
    }

}
