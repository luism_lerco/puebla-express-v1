<?php
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->model('Catalogos_model', 'catalogos');
        $this->load->model('Viajes_model', 'viajes');
        $this->load->model('Repartos_model', 'repartos');
    }

    public function getPackagesFromTravel_get($id_travel)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $viaje = $this->viajes->getTravelById($id_travel);
            $data['envios'] = $this->viajes->getPackagesFromTravel($id_travel);
            $data['viaje'] = $viaje;
            $this->response($data, 200);
            $this->load->library('Pdf');
            $pdf = new TCPDF("P", PDF_UNIT, "LETTER", TRUE, 'UTF-8', FALSE);
            $pdf->SetTitle('Reporte de viaje '. $viaje->fecha_salida);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            $pdf->AddPage();

            $pdf->Image('app-assets/img/logo_pe.jpg', 5, 5, 35, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
            $html = $this->load->view('formatos/reporteViaje', $data, true);
            $pdf->writeHTML($html, true, 0, true, 0);
            $pdf->lastPage();
            $pdf->Output("reporte" . '.pdf', 'I');

        } else {
            $this->response(null, 401);
        }

    }

    public function delivery_deals_get($id_reparto) {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $reparto = $this->repartos->getRepartoByID($id_reparto);
            $data['envios'] = $this->repartos->getDeliveryDeals($id_reparto);
            $data['reparto'] = $reparto;
            $this->response($data, 200);
            $this->load->library('Pdf');
            $pdf = new TCPDF("P", PDF_UNIT, "LETTER", TRUE, 'UTF-8', FALSE);
            $pdf->SetTitle('Reporte de reparto '. $reparto->fecha_salida);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            $pdf->AddPage();

            $pdf->Image('app-assets/img/logo_pe.jpg', 5, 5, 35, 8, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
            $html = $this->load->view('formatos/reporteReparto', $data, true);
            $pdf->writeHTML($html, true, 0, true, 0);
            $pdf->lastPage();
            $pdf->Output("reparto" . '.pdf', 'I');
        } else {
            $this->response(null, 401);
        }
    }

}