<?php

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

class Reparto extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $logueo = $this->session->userdata('logeado_pueblaexpress');
        if ($logueo != 1) {
            redirect(base_url(), 'refresh');
        }
    }

    public function reparto_view()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('reparto/list');
            $this->load->view('footer');
        } else {
            $this->response(null, 401);
        }
    }

}