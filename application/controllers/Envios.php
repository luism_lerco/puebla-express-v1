<?php

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

class Envios extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->model('HistorialMovimientos_model', 'historialMovimientosModel');
        $this->load->model('Estatus_model', 'estatusModel');
        $this->load->model('Catalogos_model', 'catalogos');
        $this->load->model('Viajes_model', 'viajes');
        $this->load->model('Envios_model', 'envios');
        $this->load->model('Operaciones_model', 'operaciones');
    }


    public function index_get()
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
            $this->load->model('Catalogos_model', 'catalogos');
            $this->response("index_get", 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function lookupByViaje_get($idViaje)
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->viajes->getPackagesFromTravel($idViaje);
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function existsByIdViaje_get($idViaje)
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->envios->existsByIdViaje($idViaje);
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function viajes_get($id)
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
            $this->load->model('Catalogos_model', 'catalogos');
            $data = array('viaje: ' . $id);
            $this->response($data);
        } else {
            $this->response(null, 401);
        }
    }

    public function getHistorialMovimientos_get($id)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->historialMovimientosModel->getAllByEnvioId($id);
            $this->response($data);
        } else {
            $this->response(null, 401);
        }
    }

    public function getAvailableStatus_get()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->estatusModel->getAll();
            $this->response($data);
        } else {
            $this->response(null, 401);
        }
    }

    public function updateStatus_post($id)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $estatus = $this->catalogos->getItemCatalogo("estatus", $this->post('estatus'));
            $envio = $this->catalogos->getItemCatalogo("envios", $id);
            $data["estatus"] = $this->post('estatus');
            $data["paqueteria"] = $this->post('paqueteria');
            $data["no_guia"] = $this->post('numGuia');
            $data["viaje_id"] = $this->post('idViaje');
            $data["reparto_id"] = $this->post('idReparto');
            if ($envio != null) {
                log_message("INFO", "Existe envio");
                if ($envio->estatus === $this->post('estatus')) {
                    log_message('DEBUG', 'Estatus iguales verificando informacion');
                    if ($this->post('estatus') == '8') {
                        if ($envio->paqueteria === $this->post('paqueteria') && $envio->no_guia === $this->post('numGuia')) {
                            log_message('DEBUG', 'Paqueteria y num de guia igual, no se inserta en historial de movimienos');
                        } else {
                            log_message('DEBUG', 'Paqueteria o numero de guia diferentes, insertando en historial de movimientos');
                            $this->historialMovimientosModel->saveMoveStatus($this->session->userdata("id_usuario"), $id, $estatus->nombre);
                        }
                    }
                    if ($this->post('estatus') == '5') {
                        if ($envio->viaje_id === $this->post('idViaje')) {
                            log_message('DEBUG', 'Mismo id de viajes, no se inserta en historial');
                        } else {
                            log_message('DEBUG', 'Id de viaje diferente, insertando en historial de movimientos');
                            $this->historialMovimientosModel->saveMoveStatus($this->session->userdata("id_usuario"), $id, $estatus->nombre);
                        }
                    }
                    $this->verificarReparto($id, $this->post('estatus'), $this->post('idReparto'), $envio->reparto_id, $estatus->nombre);
                } else {
                    $this->historialMovimientosModel->saveMoveStatus($this->session->userdata("id_usuario"), $id, $estatus->nombre);
                }
                if($this->isInBodega($this->post('estatus'))){
                    log_message("INFO", "si estoy en bodega");
                    $data = $this->unasignPackageFromReparto($data);
                }
                $result = $this->operaciones->updateTable($id, $data, "envios");
                $this->response(null, 200);
            } else {
                log_message("INFO", "No existe envio");
                $this->response(null, 404);

            }

        } else {
            $this->response(null, 401);
        }
    }

    public function verificarReparto($id, $postStatus, $postIdReparto, $repartoId, $movimiento)
    {
        if ($postStatus == '9') {
            if ($postIdReparto === $repartoId) {
                log_message('DEBUG', 'Mismo id de reparto, no se inserta en historial');
            } else {
                log_message('DEBUG', 'Id de reparto diferente, insertando en historial de movimientos');
                $this->historialMovimientosModel->saveMoveStatus($this->session->userdata("id_usuario"), $id, $movimiento);
            }
        }
    }

    public function unasignPackageFromReparto($data)
    {
        log_message("INFO", "Actualizando a 0 reparto id");
        $data["reparto_id"] = 0;
        log_message("INFO", "--> ".$data["reparto_id"]);
        return $data;
    }

    public function isInBodega($statusId)
    {
        if ($statusId === '6' || $statusId === '7') {
            log_message('info','Esta en bodega.. retorna true');
            return true;
        } else {
            return false;
        }

    }

    public function getEmployeeById_get($id)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->catalogos->getItemCatalogo("empleados", $id);
            $this->response($data);
        } else {
            $this->response(null, 401);
        }
    }

    public function verifyLastStatus_get($idEnvio)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $last_moved = $this->historialMovimientosModel->getLastMovement($idEnvio);
            $this->response($last_moved);
        }
    }

    public function get_status_list_get()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->estatusModel->getAll();
            $this->response($data);
        } else {
            $this->response(null, 401);
        }
    }
}