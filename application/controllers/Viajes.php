<?php

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

class Viajes extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->model('Catalogos_model', 'catalogos');
        $this->load->model('Viajes_model', 'viajes');
    }

    public function index_get()
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->viajes->getAll();
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function infoByViaje_get($id)
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->viajes->getAll();
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function index_post()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data["fecha_salida"] = $this->post("fecha_salida");
            $data["fecha_llegada"] = $this->post("fecha_llegada");
            $data["placas"] = $this->post("placas");
            $data["conductor"] = $this->post("conductor");
            $data["observaciones"] = $this->post("observaciones");
            $this->viajes->save($data);
            $this->response(null, 202);
        } else {
            $this->response(null, 401);
        }
    }

    public function index_patch()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $id = $this->patch("id");
            $data["fecha_salida"] = $this->patch("fecha_salida");
            $data["fecha_llegada"] = $this->patch("fecha_llegada");
            $data["placas"] = $this->patch("placas");
            $data["conductor"] = $this->patch("conductor");
            $data["observaciones"] = $this->patch("observaciones");
            $this->viajes->updateViaje($id, $data);
            $this->response(null, 202);
        } else {
            $this->response(null, 401);
        }
    }

    public function index_delete($id)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            log_message('info', "index_delete " . $id);
            $this->viajes->deleteViaje($id);
            $this->response(null, 202);
        } else {
            $this->response(null, 401);
        }
    }

    public function getAvailableTravels_get()
    {

        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->viajes->getAvailablesTravels();
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function getPackagesFromTravel_get($id_travel)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->viajes->getPackagesFromTravel($id_travel);
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }

    }

    public function searchByDates_get($field, $dateInit, $dateEnd)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->viajes->searchTravelsByDates($field, $dateInit, $dateEnd);
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function deleteSending_delete($id)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $this->viajes->deleteSendingOfTravel($id);
            $this->response("asdasd", 200);
        } else {
            $this->response(null, 401);
        }
    }


}