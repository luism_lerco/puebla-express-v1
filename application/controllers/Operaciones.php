<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Operaciones extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);

        $logueo = $this->session->userdata('logeado_pueblaexpress');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }

        $this->load->model('Operaciones_model', 'model');
        $this->load->model('Catalogos_model', 'catalogos');
        $this->load->model('HistorialMovimientos_model', 'historialMovimientosModel');
    }

    public function envios() {

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/envios');
        $this->load->view('footer');
    }

    public function reportesViajes() {

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/viajes/viajes');
        $this->load->view('footer');
    }

    public function nuevo_envio() {
        $data["sucursales"]=$this->model->getCatalogo("sucursales");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/form_envio',$data);
        $this->load->view('footer');
    }

    public function edit_envio($idEnvio) {
        $dataEnvio["envio_detail"] = $this->model->getDetalleEnvioById($idEnvio);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/form_envio', $dataEnvio);
        $this->load->view('footer');
    }

    public function escaneo() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/escaneo');
        $this->load->view('footer');
    }

    public function escaneoLote() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/escaneo/escaneo_lote');
        $this->load->view('footer');
    }
    public function getCostoLibraTierra(){
        $id_sucursal=$this->session->userdata("sucursal_id");
        echo $this->model->getCostoLibraTierra($id_sucursal);

    }

    public function getCostoLibraAvion(){
        $id_sucursal=$this->session->userdata("sucursal_id");
        echo $this->model->getCostoLibraAvion($id_sucursal);

    }

    public function searchCliente(){
        $tipos=json_decode($this->input->post('tipo_criterios'));
        $criterios=json_decode($this->input->post('criterios'));

        $sql="SELECT * FROM clientes WHERE ";
        $sql.="$tipos[0] like '%$criterios[0]%'";
        for($i=1; $i< sizeof($tipos);$i++) {
            $sql.=" AND $tipos[$i] like '%$criterios[$i]%'";
        }
//        echo $sql; die;
        $rows=$this->model->searchCliente($sql);

        $html="";
        foreach ($rows as $row){
            $html.="<tr>"
                    . "<td>$row->id</td>"
                    . "<td>$row->nombre $row->apaterno $row->amaterno</td>"
                    . "<td>$row->telefono</td>"
                    . "<td>$row->correo</td>"
                    . "<td>$row->direccion</td>"
                    . "<td>$row->pais</td>"
                    . "<td><button type='button' class='btn btn-sm btn-success' onclick='selecciona_cliente(this)'>Seleccionar</button></td>"
                . "</tr>";
        }

        echo $html;
    }

    public function insertOrUpdateEnvio() {
        $dataEnvio = $this->input->post();
        if(!isset($dataEnvio['id_envio'])) {
            // Insert envio
            $estatus = $this->catalogos->getItemCatalogo("estatus", "1");

            $data=$this->input->post();
            log_message('debug',$data['sucursal_id']);
            $clave="X";
            $sucursal=$this->model->getItemCatalogo("sucursales",$data['sucursal_id']);
//        foreach ($sucursalRequest as $sucursal){
//            $id_sucursal = $sucursal->id;
//            $clave=$sucursal->clave;
//        }
            $no=($this->model->noRegistros("envios"))+1;

            $cantidades=$data['cantidad_paquetes'];
            unset($data['cantidad_paquetes']);

            $descripciones=$data["descripcion_paquetes"];
            unset($data["descripcion_paquetes"]);

            $data["seguro"]= $this->input->post("seguro");
            $data["fecha"]=date("Y-m-d");

            if(isset($sucursal->clave)){
                $clave=$sucursal->clave;
            }

            $data["folio"]=str_pad($no, 7, "0", STR_PAD_LEFT)."-".$clave;

            //$data["total_precio"]=$data["total_valor"]*$data["total_valor"]

            $id_envio=$this->model->insertEnvio($data);

            if($id_envio>0){
                $this->historialMovimientosModel->saveMoveStatus($this->session->userdata("id_usuario"), $id_envio, $estatus->nombre);
                for($i=0; $i<sizeof($cantidades); $i++){
                    if($cantidades[$i]>0){
                        $this->model->insertPaquete(array
                            ("cantidad"=>$cantidades[$i],
                                "descripcion"=>$descripciones[$i],
                                "envio_id"=>$id_envio)
                        );
                    }
                }
                echo $id_envio;
            }
            else{
                echo 0;
            }
        } else {
            // Update envio
            $idEnvio = $dataEnvio['id_envio'];
            $updateEnvio = array(
                'tipo_envio' => $dataEnvio['tipo_envio'],
                'sucursal_id' => $dataEnvio['sucursal_id'],
                'cliente_envia' => $dataEnvio['cliente_envia'],
                'cliente_recibe' => $dataEnvio['cliente_recibe'],
                'total_valor' => $dataEnvio['total_valor'],
                'total_precio' =>$dataEnvio['total_precio'],
                'no_piezas' => $dataEnvio['cantidad_paquetes'][0],
                'observaciones' => $dataEnvio['observaciones'],
                'seguro' => $dataEnvio['seguro'],
                'total_peso' => $dataEnvio['total_peso'],
            );
            $queryResultEnvio = $this->model->updateEnvioById($updateEnvio, $idEnvio);
            if($queryResultEnvio) {
                $data_paquete = array(
                    'cantidad' => $dataEnvio['cantidad_paquetes'][0],
                    'descripcion' => $dataEnvio['descripcion_paquetes'][0]
                );
                $resultPaquetes = $this->model->updatePaqueteByEnvioId($data_paquete, $idEnvio);
                log_message('debug', 'resultado query paquetes '.$resultPaquetes);
                echo $idEnvio;
            } else {
                echo 0;
            }

        }
    }

//    public function getEnvios($estatus) {
//        $envios = $this->model->getEnvios($estatus);
//        
//        foreach ($envios as $envio) {
//            $envio->total_precio=0;
//            
//            $paquetes=$this->model->getPaquetes($envio->id_envio);
//            $str="<table style='font-size= 10px'>";
//            $str.="<tr><td width='50%'><h5>Datos quien envía</h5>"
//                    . "<dl class='row'>"
//                    . "<dt class='col-sm-3'>Cliente</dt><dd class='col-sm-9'>$envio->cliente_e</dd>"
//                    . "<dt class='col-sm-3'>Teléfono</dt><dd class='col-sm-9'>$envio->tel_e</dd>"
//                    . "<dt class='col-sm-3'>Correo</dt><dd class='col-sm-9'>$envio->correo_e</dd>"
//                    . "</dl></td>"
//                    . "<td width='50%'><h5>Datos quien recibe</h5><dl class='row'>"
//                    . "<dt class='col-sm-3'>Cliente</dt><dd class='col-sm-9'>$envio->cliente_r</dd>"
//                    . "<dt class='col-sm-3'>Teléfono</dt><dd class='col-sm-9'>$envio->tel_r</dd>"
//                    . "<dt class='col-sm-3'>Correo</dt><dd class='col-sm-9'>$envio->correo_r</dd>"
//                    . "<dt class='col-sm-3'>Dirección</dt><dd class='col-sm-9'>$envio->direccion_r</dd>"
//                    . "</dl></td></tr></table>";
//            $str.="<a data-toggle='collapse' data-target='#pq_$envio->id_envio'>Paquetes del Envío</a><div id='pq_$envio->id_envio' class='text-center collapse'><table style='margin-left: 15%; width:70%' class='table table-condensed' style=''><tr><th>Cantidad</th><th>Descripción</th></tr>";
//            foreach($paquetes as $paquete){
//                $str.="<tr><td>$paquete->cantidad</td><td>$paquete->descripcion</td></tr>";
//            }
//            $str.="</table></div>";
//            $envio->info_secundaria=$str;
//        }
//        
//        $json_data = array("data" => $envios);
//        echo json_encode($json_data);
//    }

    public function getData_envios() {

        $params=$this->input->post();

        $envios = $this->model->getEnvios($params);

        $totalRecords=$envios->num_rows();

        $json_data = array(
            "draw"            => intval( $params['draw'] ),
            "recordsTotal"    => intval( $totalRecords ),
            "recordsFiltered" => intval($totalRecords),
            "data"            => $envios->result(),
            "query"           =>$this->db->last_query()
            );
        echo json_encode($json_data);
    }

    public function searchEnvio($id) {
        log_message("INFO", "Id de envio a buscar: ".$id);
        $envio=$this->model->getEnvio($id);
        if(empty($envio)){
            echo 0; die();
        }

        $paquetes= $this->model->getPaquetes($id);
        $envio->paquetes="";
        foreach ($paquetes as $p) {
            $envio->paquetes.="<tr><td>$p->cantidad</td><td>$p->descripcion</td></tr>";
        }

        $json_data = array($envio);
        echo json_encode($json_data);

    }


    public function searchEnvioByFolio($folio) {
//        $id= $this->input->post('id');
        log_message("INFO","Folio a buscar: ".$folio);
        $envio=$this->model->getEnvioByFolio($folio);
        if(empty($envio)){
            echo 0; die();
        }
        $paquetes= $this->model->getPaquetes($envio->id_envio);
        $envio->paquetes="";
        foreach ($paquetes as $p) {
            $envio->paquetes.="<tr><td>$p->cantidad</td><td>$p->descripcion</td></tr>";
        }

        $json_data = array($envio);
        echo json_encode($json_data);

    }

    public function avanzarEstatus(){
        $id=$this->input->post("id");
        $estatus=$this->input->post("estatus");
        $permisos=$this->model->getPermisosEstatus();

        $tiene_permiso=0;
        switch ($estatus){
            case 2:
                if($permisos->estatus_proceso1){
                    $tiene_permiso=1;
                }
                break;
            case 3:
                if($permisos->estatus_matriz){
                    $tiene_permiso=1;
                }
                break;
            case 4:
                if($permisos->estatus_proceso2){
                    $tiene_permiso=1;
                }
                break;
            case 5:
                if($permisos->estatus_finalizado){
                    $tiene_permiso=1;
                }
                break;
        }

        if($tiene_permiso){
             $data=array(
                "estatus"=>$estatus,
            );

            if($estatus==4){
                $data["es_externo"]= $this->input->post("es_externo");
                $data["envio_a"]= $this->input->post("envio_a");
                if($data["es_externo"]==1){
                    $data["paqueteria"]= $this->input->post("paqueteria");
                    $data["no_guia"]= $this->input->post("no_guia");
                }
            }
            echo $this->model->updateTable($id,$data,"envios");
        }
        else{
            echo -1;
        }

    }

}
