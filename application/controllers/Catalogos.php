<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogos extends CI_Controller {

    //CONTROLADOR CATALOGOS: listados,altas y edicion

    public function __construct() {
        parent::__construct();
        error_reporting(0);
        $logueo = $this->session->userdata('logeado_pueblaexpress');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }
        
        $this->load->model('Catalogos_model', 'model');
    }

    //VIEWS - LISTADOS -------------------------------------------------------

    public function sucursales() {
        $data['sucursales'] = $this->model->getCatalogo("sucursales");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/sucursales', $data);
        $this->load->view('footer');
    }

    public function empleados() {
        $data['sucursales'] = $this->model->getCatalogo("sucursales");
        $data['empleados'] = $this->model->getCatalogo("empleados");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/empleados', $data);
        $this->load->view('footer');
    }

    public function clientes() {
        $data['clientes'] = $this->model->getCatalogo("clientes");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/clientes', $data);
        $this->load->view('footer');
    }

    public function destinos() {
        $data['destinos'] = $this->model->getCatalogo("destinos");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/destinos', $data);
        $this->load->view('footer');
    }

    //VIEWS - ALTAS ----------------------------------------------------------

    public function alta_sucursal() {
        $data['empleados'] = $this->model->getCatalogo("empleados");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_sucursal',$data);
        $this->load->view('footer');
    }

    public function alta_empleado() {
        $data['sucursales'] = $this->model->getCatalogo("sucursales");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_empleado',$data);
        $this->load->view('footer');
    }

    public function alta_cliente() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_cliente');
        $this->load->view('footer');
    }

    public function alta_destino() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_destino');
        $this->load->view('footer');
    }

    //VIEWS - EDICION ----------------------------------------------------------

    public function edicion_sucursal($id) {
        $data['empleados'] = $this->model->getCatalogo("empleados");
        $data['sucursal'] = $this->model->getItemCatalogo("sucursales", $id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_sucursal', $data);
        $this->load->view('footer');
    }

    public function edicion_empleado($id) {
        $data['empleado'] = $this->model->getItemCatalogo("empleados", $id);
        //desencriptar password
        $this->load->library('encrypt');
        $key = 'mangoo-security';
        $data['empleado']->password = $this->encrypt->decode($data['empleado']->password, $key);
        $data['permisos'] = $this->model->getCatalogoWhere("permisos", "empleados_id='$id'")[0];
        $data['sucursales'] = $this->model->getCatalogo("sucursales");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_empleado', $data);
        $this->load->view('footer');
    }

    public function edicion_cliente($id) {
        $data['cliente'] = $this->model->getItemCatalogo("clientes", $id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_cliente', $data);
        $this->load->view('footer');
    }

    public function edicion_destino($id) {
        $data['destino'] = $this->model->getItemCatalogo("destinos", $id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_destino', $data);
        $this->load->view('footer');
    }

    //Funciones genericas de insercion y edicion ------------------------
    public function insertUpdateToCatalogo($catalogo) {
        $data = $this->input->post();
        if (!isset($data['id'])) {
            //insert
            $result = $this->model->insertToCatalogo($data, $catalogo);
        }
        else{
            //update
            $id=$data['id'];
            unset($data['id']);
            $result = $this->model->updateCatalogo($data,$id, $catalogo);
        }
        
        echo $result;
    }
    
    

    //Funciones Alta y Edicion de EMPLEADO ----------------------------

    public function insertUpdateEmpleado() {
        $result = 0;

        //arrays de insercion
        $data = $this->input->post();
        $permisos = array(
            'permiso_personal' => $this->input->post('permiso_personal'),
            'permiso_clientes' => $this->input->post('permiso_clientes'),
            'permiso_sucursales' => $this->input->post('permiso_sucursales'),
            'permiso_envios' => $this->input->post('permiso_envios'),
            'permiso_editar_envio' => $this->input->post('permiso_editar_envio'),
            'permiso_escaneo' => $this->input->post('permiso_escaneo'),
            'permiso_reportes' => $this->input->post('permiso_reportes'),
            'estatus_proceso1' => $this->input->post('estatus_proceso1'),
            'estatus_matriz' => $this->input->post('estatus_matriz'),
            'estatus_proceso2' => $this->input->post('estatus_proceso2'),
            'estatus_finalizado' => $this->input->post('estatus_finalizado'),
        );

        unset($data['permiso_personal']);
        unset($data['permiso_clientes']);
        unset($data['permiso_sucursales']);
        unset($data['permiso_envios']);
        unset($data['permiso_editar_envio']);
        unset($data['permiso_escaneo']);
        unset($data['permiso_reportes']);
        unset($data['estatus_proceso1']);
        unset($data['estatus_matriz']);
        unset($data['estatus_proceso2']);
        unset($data['estatus_finalizado']);

        //encriptar password
        $this->load->library('encrypt');
        $key = 'mangoo-security';
        $data['password'] = $this->encrypt->encode($data['password'], $key);

        if (!isset($data['id'])) {
            //insertar
            $empleado_id = $this->model->insertEmpleado($data);
            if ($empleado_id > 0) { //se inserto el empleado ahora se insertan los permisos
                $permisos['empleados_id'] = $empleado_id;
                $result = $this->model->insertToCatalogo($permisos, "permisos");
            }
        }
        else{
            //editar
            $result=$this->model->updateCatalogo($data, $data['id'], 'empleados');
            if($result==1){
                $result=$this->model->updatePermisos($permisos,$data['id']);
            }
        }

        echo $result;
    }

    //Funciones de Obtención de registros --------------------------------

    public function getEstados() {
        $pais = $this->input->post("pais");
        $rows = $this->model->getCatalogoWhere("estados", "pais='$pais'");
        $result = "";
        foreach ($rows as $row) {
            $result .= "<option value='$row->id'>$row->estado</option>";
        }
        echo $result;
    }

    public function getSucursales() {
        $sucursales = $this->model->getSucursales();
        $json_data = array("data" => $sucursales);
        echo json_encode($json_data);
    }

    public function getEmpleados() {
        $empleados = $this->model->getCatalogo("empleados");
        $json_data = array("data" => $empleados);
        echo json_encode($json_data);
    }

    public function getClientes() {
        $clientes = $this->model->getClientes();
        $json_data = array("data" => $clientes);
        echo json_encode($json_data);
    }

    public function getDestinos() {
        $destinos = $this->model->getCatalogo("destinos");
        $json_data = array("data" => $destinos);
        echo json_encode($json_data);
    }

    public function getEmpleadoById($id) {
        $empleado = $this->model-getItemCatalogo("empleados",$id);
        $json_data = array("data" => $empleado);
        echo json_encode($json_data);
    }
    

}
