<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ViajesUi extends CI_Controller
{

    //CONTROLADOR CATALOGOS: listados,altas y edicion

    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $logueo = $this->session->userdata('logeado_pueblaexpress');
        if ($logueo != 1) {
            redirect(base_url(), 'refresh');
        }
    }

    public function viajes_list_view()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('viajes/list');
            $this->load->view('footer');
        } else {
            $this->response(null, 401);
        }
    }

}
