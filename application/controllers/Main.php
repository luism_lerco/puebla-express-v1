<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function index()
    {
        $this->load->view('login');
    }

    public function inicio()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('inicio');
            $this->load->view('footer');
        } else {
            redirect(base_url(), 'refresh');
        }

    }

    public function iniciar_sesion()
    {
        $this->load->model('Catalogos_model', 'model');
        $usuario = $this->input->post('usuario');
        $pass = $this->input->post('pass');

        $this->load->library('encrypt');
        $key = 'mangoo-security';

        $user = $this->model->userLogin($usuario, $pass);

        $pass_decript = $this->encrypt->decode($user->password, $key);

        if (isset($user->id)) {
            if ($pass == $pass_decript) {
                $permisos = $this->model->getPermisos($user->id);
                $data = array(
                    'logeado_pueblaexpress' => true,
                    'id_usuario' => $user->id,
                    'usuario' => $user->usuario,
                    'sucursal_id' => $user->sucursal_id,
                    'p_personal' => $permisos->permiso_personal,
                    'p_sucursales' => $permisos->permiso_sucursales,
                    'p_clientes' => $permisos->permiso_clientes,
                    'p_envios' => $permisos->permiso_envios,
                    'p_editar_envio' => $permisos->permiso_editar_envio,
                    'p_escaneo' => $permisos->permiso_escaneo,
                    'p_reportes' => $permisos->permiso_reportes,
//                    'p_reparto' => $permisos->permiso_reportes
                );
                $this->session->set_userdata($data);
                echo "ok";
            } else {
                echo "contraseña incorrecta";
            }
        } else {
            echo "no existe usuario";
        }

    }

    public function cerrar_sesion()
    {
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
}
