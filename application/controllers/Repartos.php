<?php
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

class Repartos extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->model('Repartos_model', 'repartos');
    }

    public function index_get()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->repartos->getAll();
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function index_post()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data['fecha_salida'] = $this->post("fecha_salida");
            $data["bodega_salida"] = $this->post("bodega_salida");
            $data["conductor"] = $this->post("conductor");
            $data["descripcion"] = $this->post("descripcion");
            $this->repartos->save($data);
            $this->response(null, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function index_patch()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $id = $this->patch("id_reparto");
            $data['fecha_salida'] = $this->patch("fecha_salida");
            $data["bodega_salida"] = $this->patch("bodega_salida");
            $data["conductor"] = $this->patch("conductor");
            $data["descripcion"] = $this->patch("descripcion");
            $this->repartos->updateReparto($id, $data);
            $this->response(null, 202);
        } else {
            $this->response(null, 401);
        }
    }

    public function deleteReparto_delete($id)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            log_message('info', 'borrando reparto con id: ' . $id);
            $data = $this->repartos->getPackagesFromReparto($id);
            log_message('info', 'Respuesta de getPackagesFromReparto: ' . $data);
            if ($data > 0) {
                log_message('info', '$data es mayo a 0');
                $this->response('Reparto con envios, no se puede eliminar', 400);
            } else {
                $this->repartos->deleteReparto($id);
                $this->response(null, 200);
            }
        } else {
            $this->response(null, 401);
        }
    }

    public function availableTravels_get()
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->repartos->getAvailableTravels();
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function searchByDates_get($field, $dateInit, $dateEnd)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->repartos->searchRepartosByDates($field, $dateInit, $dateEnd);
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function search_reparto_by_id_envio_get($id)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->repartos->repartoByIdEnvio($id);
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

    public function getPackagesByReparto_get($id_reparto)
    {
        if ($this->session->userdata('logeado_pueblaexpress')) {
            $data = $this->repartos->getPackagesFromReparto($id_reparto);
            $this->response($data, 200);
        } else {
            $this->response(null, 401);
        }
    }

}