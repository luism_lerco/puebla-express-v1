<?php

class HistorialMovimientos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllByEnvioId($envioId)
    {
        ///SELECT id_envio, movimiento,fecha,nombre
        //FROM historial_movimientos, empleados
        //WHERE id_envio = 17
        //AND historial_movimientos.id_empleado = empleados.id;
        $sql = "SELECT id_envio, movimiento,fecha,nombre FROM historial_movimientos, empleados WHERE id_envio = $envioId AND historial_movimientos.id_empleado = empleados.id order by fecha desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function saveMoveStatus($idEmpleado, $idEnvio, $movimiento)
    {
        date_default_timezone_set('UTC');
        $date = $date = date('Y-m-d H:i:s');
        $sql = "INSERT INTO historial_movimientos(id_empleado,id_envio,movimiento,fecha) values($idEmpleado, $idEnvio,'$movimiento','$date')";
        $query = $this->db->query($sql);

    }

    public function getLastMovement($idEnvio)
    {
        $sql = "SELECT * FROM historial_movimientos WHERE id_envio=$idEnvio order by id desc limit 1 ;";
        $query = $this->db->query($sql);
        return $query->result();
    }
}