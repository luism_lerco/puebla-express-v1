<?php
/**
 * Created by IntelliJ IDEA.
 * User: qrsof
 * Date: 14/12/18
 * Time: 18:54
 */

class Repartos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $sql = "SELECT * FROM repartos";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getAvailableTravels()
    {
        $sql = "SELECT * FROM repartos WHERE fecha_salida>=curdate() order by fecha_salida asc;";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function save($repartoData)
    {
        return $this->db->insert("repartos", $repartoData);
    }

    public function updateReparto($id, $data)
    {
        $this->db->set($data);
        $this->db->where("id_reparto", $id);
        return $this->db->update('repartos');
    }

    public function deleteReparto($id)
    {
        $sql = "DELETE FROM repartos WHERE id_reparto=$id";
        $query = $this->db->query($sql);
//        return $query->result();

    }

    public function searchRepartosByDates($field, $dateInit, $dateEnd)
    {
        $sql = "SELECT * FROM repartos WHERE $field BETWEEN '$dateInit' AND '$dateEnd' order by fecha_salida desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getRepartoByID($id_reparto){
        $sql = "SELECT * FROM repartos WHERE repartos.id_reparto = '$id_reparto'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDeliveryDeals($id_reparto) {
        $sql = "select * from envios 
                join repartos on repartos.id_reparto = envios.reparto_id
                join clientes on clientes.id = envios.cliente_recibe
                where envios.reparto_id = '$id_reparto'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function repartoByIdEnvio($idEnvio) {
        $sql = "SELECT repartos.* FROM envios JOIN repartos ON envios.reparto_id = repartos.id_reparto where id='idEnvio'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPackagesFromReparto($idReparto)
    {
        log_message('info','Dentro de model de repartos');
        $sql = "SELECT COUNT(*) as total FROM envios WHERE reparto_id=$idReparto";
        log_message('info','query a ejecutar: '.$sql);
        $query = $this->db->query($sql);
        return $query->row()->total;
    }

}