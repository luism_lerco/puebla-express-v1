<?php

class Operaciones_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    //funcion generica de obtencion de istado de un catalogo
    public function getCatalogo($catalogo)
    {
        $sql = "SELECT * FROM $catalogo";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //funcion generica
    public function getItemCatalogo($catalogo, $id)
    {
        $sql = "SELECT * FROM $catalogo WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    //funcion generica de actualizacion de tablas
    public function updateTable($id, $data, $tabla)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('' . $tabla);
    }

    public function noRegistros($tabla)
    {
        return $this->db->count_all("$tabla");
    }

    public function searchCliente($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getCostoLibraTierra($id_sucursal)
    {
        $sql = "SELECT costo_libra_tierra FROM sucursales WHERE id=$id_sucursal";
        $query = $this->db->query($sql);
        return $query->row()->costo_libra_tierra;
    }

    public function getCostoLibraAvion($id_sucursal)
    {
        $sql = "SELECT costo_libra_avion FROM sucursales WHERE id=$id_sucursal";
        $query = $this->db->query($sql);
        return $query->row()->costo_libra_avion;
    }

    public function insertEnvio($data)
    {
        $this->db->insert('envios', $data);
        return $this->db->insert_id();
    }

    public function insertPaquete($data)
    {
        return $this->db->insert("paquetes", $data);
    }

    public function getSucursalByUserId($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }
//    public function getEnvios($estatus){
//        $sql = "SELECT "
//                . "e.id as id_envio,e.no_piezas,e.fecha,e.total_peso,e.total_valor,e.costo_libra,e.observaciones,e.folio," //datos del envio
//                . "s.sucursal," //datos de la sucursal
//                . "CONCAT(ce.nombre,' ',ce.apaterno,' ',ce.amaterno) as cliente_e," //datos del cliente envia
//                . "ce.telefono as tel_e,ce.correo as correo_e,ce.direccion as direccion_e,ce.coordenadas," 
//                . "CONCAT(cr.nombre,' ',cr.apaterno,' ',cr.amaterno) as cliente_r," //datos del cliente recibe
//                . "cr.telefono as tel_r,cr.correo as correo_r,cr.direccion as direccion_r,cr.coordenadas " 
//                . "FROM envios as e "
//                . "INNER JOIN sucursales as s ON e.sucursal_id=s.id "
//                . "INNER JOIN clientes as ce ON e.cliente_envia=ce.id "
//                . "INNER JOIN clientes as cr ON e.cliente_recibe=cr.id ";
//                //. "WHERE e.estatus=$estatus";
//        $query = $this->db->query($sql);
//        return $query->result();
//    }

    public function getEnvios($params)
    {
        $columns = array(
            0 => 'folio',
            1 => 'folio',
            2 => 'sucursal',
            3 => 'no_piezas',
            4 => 'e.fecha',
            5 => "CONCAT(cr.nombre,' ',cr.apaterno,' ',cr.amaterno)",
            6 => 'escr.estado',
            7 => 'total_peso',
            8 => 'total_precio',
            9 => 'folio'
        );

        $select = ""
            . "e.id as id_envio,e.no_piezas,e.fecha,e.total_peso,e.total_precio,e.total_valor,e.costo_libra,e.observaciones,e.folio, " //datos del envio
            . "s.sucursal," //datos de la sucursal
            . "CONCAT(ce.nombre,' ',ce.apaterno,' ',ce.amaterno) as cliente_e," //datos del cliente envia
            . "ce.telefono as tel_e,ce.correo as correo_e,ce.direccion as direccion_e,esce.estado as estado_e,"
            . "CONCAT(cr.nombre,' ',cr.apaterno,' ',cr.amaterno) as cliente_r," //datos del cliente recibe
            . "cr.telefono as tel_r,cr.correo as correo_r,cr.direccion as direccion_r,escr.estado as estado_r," . "paquetes.descripcion";

        $this->db->select($select);
        $this->db->from('envios as e');
        $this->db->join('sucursales as s', 'e.sucursal_id=s.id', 'left');
        $this->db->join('clientes as ce', 'e.cliente_envia=ce.id');
        $this->db->join('clientes as cr', 'e.cliente_recibe=cr.id');
        $this->db->join('estados as esce', 'ce.estado=esce.id');
        $this->db->join('estados as escr', 'cr.estado=escr.id');
        $this->db->join('paquetes as paquetes', 'e.id = paquetes.envio_id');

        //si hay busqueda por fecha
//        if($params["fecha_inicio"]!="" && $params["fecha_fin"]!=""){
//            $this->db->where('fecha_carga >=', date('Y-m-d', strtotime($params["fecha_inicio"])));
//            $this->db->where('fecha_carga <=', date('Y-m-d', strtotime($params["fecha_fin"])));
//        }
        //si hay busqueda con el campo de busqueda
        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c . '', $search);
            }
            $this->db->group_end();

        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'], $params['start']);

        //echo $this->db->get_compiled_select();
        $query = $this->db->get();
        return $query;
    }

    public function getEnvio($id)
    {
        $sql = "SELECT "
            . "e.id as id_envio, e.tipo_envio, e.no_piezas, e.fecha,e.total_peso, e.total_valor, e.total_precio, e.costo_libra, e.observaciones, e.viaje_id, e.reparto_id, "
            . "e.folio, e.estatus, e.envio_a, e.paqueteria, e.no_guia, " //datos del envio
            . "e.sucursal_id, s.sucursal,s.estado,s.pais,s.direccion as direccion_s," //datos de la sucursal
            . "CONCAT(ce.nombre,' ',ce.apaterno,' ',ce.amaterno) as cliente_e," //datos del cliente envia
            . "ce.telefono as tel_e,ce.correo as correo_e,ce.direccion as direccion_e,ce.coordenadas as coord_e,ce.estado as estado_e,"
            . "CONCAT(cr.nombre,' ',cr.apaterno,' ',cr.amaterno) as cliente_r,cr.estado as estado_c,cr.cp," //datos del cliente recibe
            . "cr.telefono as tel_r,cr.correo as correo_r,cr.direccion as direccion_r,cr.coordenadas as coord_r, "
            . "est.estado as estado_recepcion, paquetes.descripcion as descripcion, esto.estado as estado_origen, "
            . "r.fecha_salida as reparto_fecha_salida, r.bodega_salida as reparto_bodega_salida, r.conductor as reparto_conductor, r.descripcion as reparto_descripcion "
            . "FROM envios as e "
            . "LEFT JOIN sucursales as s ON e.sucursal_id=s.id "
            . "INNER JOIN clientes as ce ON e.cliente_envia=ce.id "
            . "INNER JOIN clientes as cr ON e.cliente_recibe=cr.id "
            . "JOIN estados as est ON est.id=cr.estado "
            . "JOIN estados as esto ON esto.id=ce.estado "
            . "JOIN paquetes as paquetes ON e.id = paquetes.envio_id "
            . "LEFT JOIN repartos as r ON e.reparto_id = r.id_reparto "
            . "WHERE e.id='$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getDetalleEnvioById($id) {
        $sql = "SELECT ".
        "e.id as id_envio, e.tipo_envio, e.no_piezas, e.fecha,e.total_peso, e.total_valor, e.total_precio, ".
        "e.costo_libra, e.observaciones, e.viaje_id, e.reparto_id, e.seguro, e.folio, e.estatus, e.envio_a, ".
        "e.paqueteria, e.no_guia, e.sucursal_id, ".
        "ce.id as ce_id, CONCAT(ce.nombre,' ',ce.apaterno,' ',ce.amaterno) as cliente_e, ce.telefono as tel_e, ".
        "ce.correo as correo_e, ce.direccion as direccion_e,ce.coordenadas as coord_e,ce.estado as estado_e, ".
        "cr.id as cr_id, CONCAT(cr.nombre,' ',cr.apaterno,' ',cr.amaterno) as cliente_r,cr.estado as estado_c,cr.cp, ".
        "cr.telefono as tel_r,cr.correo as correo_r,cr.direccion as direccion_r,cr.coordenadas as coord_r, ".
        "paquetes.descripcion as descripcion ".
        "FROM envios as e ".
        "INNER JOIN clientes as ce ON e.cliente_envia=ce.id ".
        "INNER JOIN clientes as cr ON e.cliente_recibe=cr.id ".
        "JOIN paquetes as paquetes ON e.id = paquetes.envio_id ".
        "WHERE e.id='$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function updateEnvioById($data, $idEnvio) {
        //log_message('debug', print_r($data, true));
        $this->db->set($data);
        $this->db->where('id', $idEnvio);
        return $this->db->update('envios');
    }

    public function updatePaqueteByEnvioId($data, $idEnvio) {
        $this->db->set($data);
        $this->db->where('envio_id',$idEnvio);
        return $this->db->update('paquetes');
    }

    public function getFolioEnvioById($id_envio) {
        $sql = "SELECT folio FROM envios where id=$id_envio";
        $query = $this->db->query($sql);
        return $query->row()->folio;
    }

    public function getEnvioByFolio($folio)
    {
        $sql = "SELECT "
        . "e.id as id_envio, e.tipo_envio, e.no_piezas,e.fecha,e.total_peso,e.total_valor,e.total_precio,e.costo_libra,e.observaciones,e.viaje_id, e.reparto_id, "
        . "e.folio,e.estatus,e.envio_a,e.paqueteria,e.no_guia, " //datos del envio
        . "e.sucursal_id,s.sucursal,s.estado,s.pais,s.direccion as direccion_s, " //datos de la sucursal
        . "CONCAT(ce.nombre,' ',ce.apaterno,' ',ce.amaterno) as cliente_e, " //datos del cliente envia
        . "ce.telefono as tel_e,ce.correo as correo_e,ce.direccion as direccion_e,ce.coordenadas as coord_e,ce.estado as estado_e, "
        . "CONCAT(cr.nombre,' ',cr.apaterno,' ',cr.amaterno) as cliente_r,cr.estado as estado_c,cr.cp, " //datos del cliente recibe
        . "cr.telefono as tel_r,cr.correo as correo_r,cr.direccion as direccion_r,cr.coordenadas as coord_r, "
        . "est.estado as estado_recepcion, paquetes.descripcion as descripcion, esto.estado as estado_origen, "
        . "r.fecha_salida as reparto_fecha_salida, r.bodega_salida as reparto_bodega_salida, r.conductor as reparto_conductor, r.descripcion as reparto_descripcion "
        . "FROM envios as e "
        . "LEFT JOIN sucursales as s ON e.sucursal_id=s.id "
        . "INNER JOIN clientes as ce ON e.cliente_envia=ce.id "
        . "INNER JOIN clientes as cr ON e.cliente_recibe=cr.id "
        . "JOIN estados as est ON est.id=cr.estado "
        . "JOIN estados as esto ON esto.id=ce.estado "
        . "JOIN paquetes as paquetes ON e.id = paquetes.envio_id "
        . "LEFT JOIN repartos as r ON e.reparto_id = r.id_reparto "
        . "WHERE e.folio='$folio'";
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function getPaquetes($id_envio)
    {
        $sql = "SELECT * FROM paquetes WHERE envio_id=$id_envio";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getMatriz($estado)
    {
        if ($estado != 0) {
            $sql = "SELECT matriz FROM estados WHERE id=$estado";
            $query = $this->db->query($sql);
            return $query->row()->matriz;
        } else {
            return "Sucursal N/A";
        }

    }

    public function getPermisosEstatus()
    {
        $sql = "SELECT estatus_proceso1,estatus_matriz,estatus_proceso2,estatus_finalizado FROM permisos WHERE empleados_id=" . $this->session->userdata("id_usuario");
        $query = $this->db->query($sql);
        return $query->row();
    }


}