<?php
/**
 * Created by IntelliJ IDEA.
 * User: qrsof
 * Date: 14/10/18
 * Time: 21:09
 */

class Viajes_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAvailablesTravels()
    {
        $sql = "SELECT * FROM viajes WHERE fecha_salida>=curdate() order by fecha_salida asc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPackagesFromTravel($id)
    {
        $sql = "SELECT envios.folio, envios.fecha, envios.total_valor,envios.total_precio, envios.no_piezas, envios.total_peso,
                envios.costo_libra, paquetes.descripcion, sucursales.comision, sucursales.sucursal, estados.matriz
                FROM envios, paquetes , sucursales, estados, clientes
                WHERE paquetes.envio_id =envios.id 
                AND envios.sucursal_id = sucursales.id 
                AND envios.cliente_recibe = clientes.id
                AND clientes.estado = estados.id
                AND envios.viaje_id = $id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function searchTravelsByDates($field, $dateInit, $dateEnd)
    {
//        $sql = "SELECT * FROM viajes WHERE $field BETWEEN $dateInit AND $dateEnd";
        $sql = "SELECT * FROM viajes WHERE $field BETWEEN '$dateInit' AND '$dateEnd' order by fecha_salida desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTravelById($id)
    {
        $sql = "SELECT id, DATE(fecha_salida) AS fecha_salida, DATE(fecha_llegada) as fecha_llegada, placas, conductor, observaciones FROM viajes WHERE id =$id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getAll()
    {
        $sql = "SELECT * FROM viajes";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function save($data)
    {
        return $this->db->insert("viajes", $data);
    }

    public function deleteSendingOfTravel($id)
    {
        $sql = "UPDATE envios SET viaje_id = null WHERE id=$id";
        $query = $this->db->query($sql);
//        return $query->result();
    }

    public function updateViaje($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('viajes');
    }

    public function deleteViaje($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('viajes');
    }
}