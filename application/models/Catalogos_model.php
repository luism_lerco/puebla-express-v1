<?php

class Catalogos_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    //funcion generica de obtencion de istado de un catalogo
    public function getCatalogo($catalogo) {
        $sql = "SELECT * FROM $catalogo WHERE estatus!=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    //funcion generica de consulta con condicion
    public function getCatalogoWhere($catalogo,$condicion) {
        $sql = "SELECT * FROM $catalogo WHERE $condicion";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    //funcion generica que obtiene un registro por ID
    public function getItemCatalogo($catalogo,$id) {
        $sql = "SELECT * FROM $catalogo WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    //funcion generica de insercion en un catalogo
    public function insertToCatalogo($data,$catalogo) {
        return $this->db->insert(''.$catalogo, $data);
    }

    //funcion generica de edicion en un catalogo
    public function updateCatalogo($data, $id, $catalogo) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update(''.$catalogo);
    }
    
    public function insertEmpleado($data) {
        $this->db->insert('empleados', $data);
        return $this->db->insert_id();
    }
    
    public function updatePermisos($data,$id){
        $this->db->set($data);
        $this->db->where('empleados_id', $id);
        return $this->db->update('permisos');
    }
    
    public function getClientes(){
        $sql = "SELECT id, CONCAT(nombre,' ',apaterno,' ',amaterno) as nombre,telefono,correo,direccion,coordenadas,estatus FROM clientes WHERE estatus!=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    // Funciones de logueo
    public function userLogin($usuario,$pass) {
        $sql = "SELECT id, usuario, password,sucursal_id FROM empleados WHERE usuario='$usuario' AND estatus=1";
        $query = $this->db->query($sql); 
        //return $sql; 
        return $query->row();
    }
    
    public function getPermisos($id) {
        $sql = "SELECT * FROM permisos WHERE empleados_id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function getSucursales(){
        $sql = "SELECT sucursales.id,sucursal,nombre as encargado,sucursales.telefono,UPPER(sucursales.pais) as pais,comision "
                . "FROM sucursales INNER JOIN empleados ON encargado=empleados.id WHERE sucursales.estatus!=0";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getSucursalByUserID($catalogo,$id) {
        $sql = "SELECT *"
            . "FROM sucursales WHERE sucursales.encargado=1";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
   
}