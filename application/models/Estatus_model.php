<?php

class Estatus_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll() {
        $sql = "SELECT * FROM estatus";
        $query = $this->db->query($sql);
        return $query->result();
    }
}