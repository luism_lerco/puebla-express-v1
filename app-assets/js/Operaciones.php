<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Operaciones extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $logueo = $this->session->userdata('logeado_pueblaexpress');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }
        
        $this->load->model('Operaciones_model', 'model');
    }
    
    public function envios() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/envios');
        $this->load->view('footer');
    }
    
    public function nuevo_envio() {
        $data["sucursales"]=$this->model->getCatalogo("sucursales");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/form_envio',$data);
        $this->load->view('footer');
    }
    
    public function escaneo() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('operaciones/escaneo');
        $this->load->view('footer');
    }
    
    public function getCostoLibra(){
        $id_sucursal=$this->input->post("sucursal");
        echo $this->model->getCostoLibra($id_sucursal);
    }
    
    public function searchCliente(){
        
        $tipos=json_decode($this->input->post('tipo_criterios'));
        $criterios=json_decode($this->input->post('criterios'));
        
        $sql="SELECT * FROM clientes WHERE ";
        $sql.="$tipos[0] like '%$criterios[0]%'";
        for($i=1; $i< sizeof($tipos);$i++) {
            $sql.=" AND $tipos[$i] like '%$criterios[$i]%'";
        }
//        echo $sql; die;
        $rows=$this->model->searchCliente($sql);
        
        $html="";
        foreach ($rows as $row){
            $html.="<tr>"
                    . "<td>$row->id</td>"
                    . "<td>$row->nombre $row->apaterno $row->amaterno</td>"
                    . "<td>$row->telefono</td>"
                    . "<td>$row->correo</td>"
                    . "<td>$row->direccion</td>"
                    . "<td>$row->pais</td>"
                    . "<td><button type='button' class='btn btn-sm btn-success' onclick='selecciona_cliente(this)'>Seleccionar</button></td>"
                . "</tr>";
        }
        
        echo $html;
    }
    
    public function saveEnvio() {
        $data=$this->input->post();
        $sucursal=$this->model->getItemCatalogo("sucursales",$data['sucursal_id']);
        $no=($this->model->noRegistros("envios"))+1;
        
        $cantidades=$data['cantidad_paquetes'];
        unset($data['cantidad_paquetes']);
        
        $descripciones=$data["descripcion_paquetes"];
        unset($data["descripcion_paquetes"]);
        
        $data["seguro"]= $this->input->post("seguro");
        
        $data["fecha"]=date("Y-m-d");
        
        $data["folio"]=str_pad($no, 7, "0", STR_PAD_LEFT)."-".$sucursal->clave;
        
        $id_envio=$this->model->insertEnvio($data);
        
        if($id_envio>0){
            for($i=0; $i<sizeof($cantidades); $i++){
                if($cantidades[$i]>0){
                    $this->model->insertPaquete(array
                        ("cantidad"=>$cantidades[$i],
                         "descripcion"=>$descripciones[$i],
                         "envio_id"=>$id_envio)
                    );
                }
            }
            echo $id_envio;
        }
        else{
            echo 0;
        }
        
    }
    
    public function getEnvios($estatus) {
        $envios = $this->model->getEnvios($estatus);
        
        foreach ($envios as $envio) {
            $envio->total_precio=0;
            
            $paquetes=$this->model->getPaquetes($envio->id_envio);
            $str="<table style='font-size= 10px'>";
            $str.="<tr><td width='50%'><h5>Datos quien envía</h5>"
                    . "<dl class='row'>"
                    . "<dt class='col-sm-3'>Cliente</dt><dd class='col-sm-9'>$envio->cliente_e</dd>"
                    . "<dt class='col-sm-3'>Teléfono</dt><dd class='col-sm-9'>$envio->tel_e</dd>"
                    . "<dt class='col-sm-3'>Correo</dt><dd class='col-sm-9'>$envio->correo_e</dd>"
                    . "</dl></td>"
                    . "<td width='50%'><h5>Datos quien recibe</h5><dl class='row'>"
                    . "<dt class='col-sm-3'>Cliente</dt><dd class='col-sm-9'>$envio->cliente_r</dd>"
                    . "<dt class='col-sm-3'>Teléfono</dt><dd class='col-sm-9'>$envio->tel_r</dd>"
                    . "<dt class='col-sm-3'>Correo</dt><dd class='col-sm-9'>$envio->correo_r</dd>"
                    . "<dt class='col-sm-3'>Dirección</dt><dd class='col-sm-9'>$envio->direccion_r</dd>"
                    . "</dl></td></tr></table>";
            $str.="<a data-toggle='collapse' data-target='#pq_$envio->id_envio'>Paquetes del Envío</a><div id='pq_$envio->id_envio' class='text-center collapse'><table style='margin-left: 15%; width:70%' class='table table-condensed' style=''><tr><th>Cantidad</th><th>Descripción</th></tr>";
            foreach($paquetes as $paquete){
                $str.="<tr><td>$paquete->cantidad</td><td>$paquete->descripcion</td></tr>";
            }
            $str.="</table></div>";
            $envio->info_secundaria=$str;
        }
        
        $json_data = array("data" => $envios);
        echo json_encode($json_data);
    }
    
    public function searchEnvio() {
        $id= $this->input->post('id');
        $envio=$this->model->getEnvio($id);
        if(empty($envio)){
            echo 0; die();
        }
        
        $paquetes= $this->model->getPaquetes($id);
        $envio->paquetes="";
        foreach ($paquetes as $p) {
            $envio->paquetes.="<tr><td>$p->cantidad</td><td>$p->descripcion</td></tr>";
        }
        
        $json_data = array($envio);
        echo json_encode($json_data);
        
    }
    
    public function avanzarEstatus(){
        $id=$this->input->post("id");
        $estatus=$this->input->post("estatus");
        $data=array(
            "estatus"=>$estatus,
        );
        
        if($estatus==4){
            $data["es_externo"]= $this->input->post("es_externo");
            $data["envio_a"]= $this->input->post("envio_a");
            if($data["es_externo"]==1){
                $data["paqueteria"]= $this->input->post("paqueteria");
                $data["no_guia"]= $this->input->post("no_guia");
            }
        }
        echo $this->model->updateTable($id,$data,"envios");
    }
    
    
}
